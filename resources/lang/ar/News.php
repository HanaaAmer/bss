<?php
return [
   
    'title'=>'العنوان',  
    'titleImage'=>'صورة العنوان',
    'short_description'=>'الوصف القصير',
    'long_description'=>'الوصف الطويل',
    'image'=>'صوره الاخبار',
   'add_News'=>'اضافه خبر جديد',
   'active'=>'مفعل',
   'all_News'=>'كل الاخبار',
   'update_News'=>'تعديل الخبر',
];