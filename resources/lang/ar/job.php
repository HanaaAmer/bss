<?php
return [
     //
     'requirements'=>'متطلبات الوظيفه',
    'jobs'=>'الوظائف',
    'job_title'=>'عنوان الوظيفة',
    'add_job'=>'إضافة وظيفة جديدة',
    'update_job'=>'تعديل الوظيفة',
    'deleteJob'=>'خذف الوظيفة',
    'all_jobs'=>'كل الوظائف',

    'name'=>'اسم الوظيفة',
    'image'=>'صورة الوظيفة',
    'description'=>'الوصف  ',
    'active'=>'مُفعل',
    'activation'=>'التفعيل',
    '1'=>'مُفعل',
    '0'=>'غير مُفعل',
    'null'=>'غير مُفعل',
    'NULL'=>'غير مُفعل',
    ''=>'غير مُفعل',
    'show_in_homepage'=>'الظهور فى الصفحات',
   
    'select_company'=>'اختار الشركه',
    
    'companyname'=>'اسم الشركه',
'AddRequirement'=>'اضافة متطلب جديد',
'requirednumber'=>'العدد المطلوب',


];