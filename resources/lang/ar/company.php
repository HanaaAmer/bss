<?php
return [
    //General
    'name'=>'الأسم',
    'title'=>'عنوان الوصف',
    'image'=>'الصورة',
    'logo'=>'اللوجو',
    'description'=>'الوصف',
    'path'      => 'لينك',
    'active'=>'مُفعل',
    'disactive'=>'غير مفعل',
    'activate'=>'تفعيل',
    'disactivate'=>'اخفاء',
    'direction'=>'اتجاه الصوره',
    'Selectdirection'=>'اختار الاتجاه',
    ];