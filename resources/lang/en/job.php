<?php
return [
     //
     'requirements'=>'Requirements',
    'jobs'=>'Jobs',
    'job_title'=>'Job Title',
   
    'add_job'=>'Add New Job',
    'update_job'=>'Update Job',
    'deleteJob'=>'Delete Job',
    'all_jobs'=>'All Jobs',
    
    'name'=>'Job Name',
    'image'=>'Job Image',
    'description'=>'Description  ',
    'active'=>'Active',
    'activation'=>'Activation',
    '1'=>'Active',
    '0'=>'Not Active',
    'null'=>'Not Active',
    'NULL'=>'Not Active',
    ''=>'Not Active',
    'show_in_homepage'=>'Show In Pages',
   
    'select_company'=>'Select Company',
    
    'companyname'=>'Company Name',
'AddRequirement'=>'Add Requirement',
'requirednumber'=>'Required Number',


];