<?php
return [
    //General
  
    'name'=>'Name',
    'title'=>'Tittle',
    'image'=>'Image',
    'logo'=>'Logo',
    'description'=>'Description',
    'activation'=>'Activation',
    'active'=>'Active',
    'disactive'=>'Deactive',
    'activate'=>'Activate',
    'disactivate'=>'Deactivate',
    'direction'=>'Direction Of Image',
    'Selectdirection' =>'Select Direction',
];