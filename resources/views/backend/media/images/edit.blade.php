@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.editAlbum')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateImage',['id'=>$images->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            @include('backend.includes.check_language')
                       @foreach (\Config::get('languages') as $locale=>$language) 
                       <div id="tap_{{$locale}}">
                           <div class="form-group">
                               <label class="control-label">{{trans('backend.albumTitle',[''],$locale)}}</label>
                               <input name="album_title:{{$locale}}" value="{{ $images->translate($locale)->album_title}}" type="text" class="form-control" placeholder="{{trans('backend.album_title',[''],$locale)}}" required>
                               @if($errors->has('album_title:{{$locale}}'))
                                   <span class="help-block">{{$errors->first('album_title:'.$locale)}}</span>
                               @endif
                           </div>
                           <div class="form-group">
                            <label class="control-label">{{trans('backend.long_description',[''],$locale)}}</label>
                            <textarea name="long_description:{{$locale}}"   class="ckeditor" placeholder="{{trans('backend.long_description',[''],$locale)}}">{!! html_entity_decode( $images->translate($locale)->long_description) !!}</textarea>
                            @if($errors->has('long_description:{{$locale}}'))
                                <span class="help-block">{{$errors->first('long_description:'.$locale)}}</span>
                            @endif
                        </div>
                            </div>
                            @endforeach
                       
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.images')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input class="form-control" type="file" name="image[]" multiple>
                                    @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('uploadNewImage')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn blue" href="{{route('imagesIndex')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                    <div class="album">
                        <label class="control-label">{{trans('backend.storedImages')}}</label>
                        <div class="row">
                            @foreach($images->image as $its)
                            <div class="col-sm-6 col-md-3">
                                <a class="thumbnail">
                                <img src="{{UPLOADS}}/images/{{$its}}" style="height: 180px; width: 100%; display: block;">
                                </a>
                                <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteSingle',['image'=>$its, 'id'=>$images->id])}}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn red">{{trans('backend.delete')}}
                                    </button>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <style>
        .album{padding:0 15px;}
        .album .col-md-3{position: relative;}
        .album .form-delete-c{
            position: absolute;
            top:50%;
            left:50%;
            transform:translateX(-50%) translateY(-50%);
        }
    </style>
    @endsection