@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('backend.addAlbum')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('imageUpload')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            @include('backend.includes.check_language')
                       @foreach (\Config::get('languages') as $locale=>$language) 
                       <div id="tap_{{$locale}}">
                           <div class="form-group">
                               <label class="control-label">{{trans('backend.albumTitle',[''],$locale)}}</label>
                               <input name="album_title:{{$locale}}" value="{{old('album_title:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('backend.album_title',[''],$locale)}}" required>
                               @if($errors->has('album_title:{{$locale}}'))
                                   <span class="help-block">{{$errors->first('album_title:'.$locale)}}</span>
                               @endif
                           </div>
                           <div class="form-group">
                            <label class="control-label">{{trans('backend.long_description',[''],$locale)}}</label>
                            <textarea name="long_description:{{$locale}}" value="{{old('long_description:'.$locale)}}" type="text" class="ckeditor" placeholder="{{trans('backend.long_description',[''],$locale)}}" required></textarea>
                            @if($errors->has('long_description:{{$locale}}'))
                                <span class="help-block">{{$errors->first('long_description:'.$locale)}}</span>
                            @endif
                        </div>
                            </div>
                            @endforeach
                        
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.images')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input class="form-control" type="file" name="image[]" multiple>
                                    @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('uploadNewImage')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn blue" href="{{route('imagesIndex')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    @endsection