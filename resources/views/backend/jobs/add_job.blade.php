@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('job.add_job')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddJob')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                        @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('job.name',[''],$locale)}}</label>
                                <input name="name:{{$locale}}" value="{{old('name:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('job.name',[''],$locale)}}" required>
                                @if($errors->has('name:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('job.job_title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{old('title:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('job.job_title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('backend.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="ckeditor" placeholder="{{trans('backend.description',[''],$locale)}}"  required>{{old('description:'.$locale)}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                            <label class="control-label">{{trans('job.requirements',[''],$locale)}}</label>
                             <div class="input-group control-group increment" >
                             <table class="table table-striped table-bordered table-hover place{{$locale}}" ><tr>
                           
                               <td>
                              <input type="text" name="requirements:{{$locale}}[]" value="{{old('requirements[]:'.$locale)}}" class="form-control" placeholder="{{trans('job.requirements',[''],$locale)}}" >
                               </td>  
                               
                                <td> 
                               <button class="btn btn-success add{{$locale}}" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                    </td> 
                                    </tr></table>
                              </div>
                        </div>
                          
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('job.requirednumber',[''],$locale)}}</label>
                                <input name="requirednumber:{{$locale}}" value="{{old('requirednumber:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('job.requirednumber',[''],$locale)}}" required>
                                @if($errors->has('requirednumber:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('requirednumber:'.$locale)}}</span>
                                @endif
                            </div>
                    </div>
                        @endforeach
                        <div class="from-group">
                           <label class="control-label">{{trans('job.companyname')}}</label>
                         
                           
                          <select name="companys_id" class="form-control" required>
                                        <option value="">--{{trans('job.select_company')}}--</option>
                                        
                                        @foreach ($companys as $item)
                                        <option value="{{$item->id }}"> {{$item->name}}</option>     
                                        @endforeach
                                         
                                    </select>
                                     </div>
                            
                            <div class="form-group">
                                    <label class="control-label">{{trans('job.image')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                        <input name="image" type="file" class="form-control" required>
                                        @if($errors->has('image'))
                                            <span class="help-block">{{$errors->first('image')}}</span>
                                        @endif
                                    </div>
                                </div>
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('job.active')}}</label>
                                <div class="input-group">
                                    <input type="checkbox" name="active" class="checkbox" value="1">
                                </div>
                            </div>
                           
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllJobs')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>

    
@endsection
@section('js')

 <script type="text/javascript">
        var i=1;
        var j=1;
        var x=1;

    $(document).ready(function() {
       
     $('.addar').click(function(){
       
         
          $('.placear').append('<tr id="row'+j+'" class="dynamic-added"><td><input type="text"  name="requirements:ar[]" value="{{old('requirements:ar')}}" placeholder=" متطلبات الوظيفه " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
          });
         $('.adden').click(function(){
          $('.placeen').append('<tr id="row'+x+'" class="dynamic-added"><td><input type="text"  name="requirements:en[]" value="{{old('requirements:en')}}" placeholder=" Requirements " class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+x+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        
     });

     $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
     });
    });
    

</script>
@endsection