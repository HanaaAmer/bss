@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
      
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe">  {{trans('backend.progressofthejob')}}</i></div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('job.name')}}</th>
                                <th>{{trans('backend.name')}}</th>
                                <th width="30%">{{trans('backend.email')}}</th>
                                <th>{{trans('backend.phone')}}</th>
                                <th>{{trans('backend.cv')}}</th>
                                <th width="1%">{{trans('backend.action')}}</th>
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jobs as $i=>$job)
                            <tr class="odd gradeX">
                                <td>{{$i+1}}</td>
                                <td>{{$job->Jobs->name}}</td>
                                <td>{{$job->name}}</td>
                                <td> {{$job->email}}</td>
                                <td>{{$job->phone}}</td>
                                <td> <a href="{{UPLOADS}}/subscription/{{$job->cv}}" class="btn primary block-lvl" download>{{ \App\Http\Controllers\HomeController::translateWord('download_cv')}}</a></td>
                               <td>
                               <div class="clearfix">
                                    <form action="{{route('updateprogress')}}" method="post">
                                        {{csrf_field()}}
                                       @if($job->active==0) <input class="btn green btn-outline" type="submit" value="Confirm">@endif
                                        <input name="id" type="hidden" value="{{$job->id}}">
                                        <input type="hidden" name="_method" value="PUT">
                                    </form>
                                    
                                </div>
                                </td>
                                <td>
                                <div class="clearfix">
                                    <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteprogessById',['proId'=>$job->id])}}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn red btn-outline">{{trans('backend.delete')}}
                                        </button>
                                    </form>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
@section('js')
<script src="{{ASSETS}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ASSETS}}/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
@endsection

@section('css')
<link href="{{ASSETS}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
@endsection