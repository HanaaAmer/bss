@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('News.update_News')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updateNewsById',['NewsId'=>$News->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            @include('backend.includes.check_language')
                            @foreach (\Config::get('languages') as $locale=>$language) 
                            <div id="tap_{{$locale}}">
                                <div class="form-group">
                                    <label class="control-label">{{trans('News.title',[''],$locale)}}</label>
                                    <input name="title:{{$locale}}" value="{{ $News->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('backend.title',[''],$locale)}}" >
                                    @if($errors->has('title:{{$locale}}'))
                                        <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('News.short_description',[''],$locale)}}</label>
                                    <textarea name="short_description:{{$locale}}" class="form-control" placeholder="{{trans('backend.short_description',[''],$locale)}}">{{ $News->translate($locale)->short_description}}</textarea>
                                    @if($errors->has('short_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('short_description'.$locale)}}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('News.long_description',[''],$locale)}}</label>
                                    <textarea name="long_description:{{$locale}}" class="ckeditor" placeholder="{{trans('backend.long_description',[''],$locale)}}">{{ $News->translate($locale)->long_description}}</textarea>
                                    @if($errors->has('long_description:{{$locale}}'))
                                        <span class="help-block">
                                        {{$errors->first('long_description'.$locale)}}
                                        </span>
                                    @endif
                                </div>
                              
                            </div>    
                            @endforeach
                            <div class="form-group">
                                <label class="control-label">{{trans('News.titleImage')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="titleImage" type="file" class="form-control">
                                    <img height="80" src="{{UPLOADS}}/News/{{$News->titleImage}}">
                                    @if($errors->has('titleImage'))
                                    <span class="help-block">{{$errors->first('titleImage')}}</span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('News.active')}}</label>
                                <div class="input-group">
                                    @if($News->active == 1)
                                    <input type="checkbox" name="active" class="checkbox"id="active"  value="1" checked>
                                    @else
                                    <input type="hidden" name="active" class="checkbox" value="0" >
                                    <input type="checkbox" name="active" class="checkbox" value="1" >
                                    @endif
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('News.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    <img height="70" src="{{UPLOADS}}/News/{{$News->image}}">
                                    @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                            </div>
                            <input type="hidden" name="_method" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <script>
            $('#active').on('change', function(){

         var y=((this.checked)==true ? 1 : 0); 
             if(y==0){
            $( '.dynam' ).append('<input  class="checkbox" name="active" value='+y+'  type="hidden"  />');    
         }  
}       ).trigger( 'change' );
</script>
    @endsection