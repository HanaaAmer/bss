@extends('backend.layouts.main_layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <a style="margin: 5px;" class="btn blue" href="{{route('getAddNews')}}">
            <i class="fa fa-pencil"></i> {{trans('News.add_News')}}
        </a>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe">  {{trans('News.all_News')}}</i></div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('News.title')}}</th>
                                <th width="30%">{{trans('News.short_description')}}</th>
                                <th>{{trans('blog.comments')}}</th>
                                <th>{{trans('News.titleImage')}}</th>                             
                                <th width="1%">{{trans('backend.action')}}</th>
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody>
                       
                            @foreach($News as $i=>$New)
                            <tr class="odd gradeX">
                            <td>{{$i+1}}</td>
                                <td>{{$New->title}}</td>
                                <td>{{$New->short_description}}</td>
                                <?php $comments = \App\Repositories\NewsCommentsRepository::NewsCommentsToView($New->id); 
      
                                ?>
                                <td><a href="{{route('getAllNewsComments', ['NewsId' => $New->id])}}">{{count($comments)}} {{trans('blog.comment')}}</a></td>
                                <td><img height="70" src="{{UPLOADS}}/News/{{$New->titleImage}}"></td>
                                <td>
                                <div class="clearfix">
                                    <a class="btn green btn-outline" href="{{route('getNewsById',['NewsId'=>$New->id])}}">
                                        {{trans('backend.update')}}
                                    </a>
                                </div>
                                </td>
                                <td>
                                <div class="clearfix">
                                    <form class="form-delete-c" method="post" onclick="return confirm('<?php echo trans('backend.confirmDelete');?>')" action="{{route('deleteNewsById',['NewsId'=>$New->id])}}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn red btn-outline">{{trans('backend.delete')}}
                                        </button>
                                    </form>
                                </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection
@section('js')
<script src="{{ASSETS}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{ASSETS}}/pages/scripts/table-datatables-colreorder.min.js" type="text/javascript"></script>
@endsection

@section('css')
<link href="{{ASSETS}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ASSETS}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
@endsection