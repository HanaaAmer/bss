@extends('backend.layouts.main_layout')
@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          {{Session::get('sucess')}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
  
  var exist = '{{Session::has('sucess')}}';
  if(exist){
    $('#myModal').modal('show');
  }
  
  
</script>
  
<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('link.update_link')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    @if ($setting != null)
                        
                   
                    <form method="post" action="{{route('updateSettings')}}">
                        {{ csrf_field() }}
                       
                          <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('setting.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('setting.description',[''],$locale)}}" maxlength="100" required>{{$setting->translate($locale)->description}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.contactus_description',[''],$locale)}}</label>
                                <textarea name="contactus_description:{{$locale}}" class="form-control" placeholder="{{trans('setting.contactus_description',[''],$locale)}}" maxlength="100" required>{{$setting->translate($locale)->contactus_description}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.address',[''],$locale)}}</label>
                                <input name="address:{{$locale}}" value="{{$setting->translate($locale)->address}}" type="text" class="form-control" placeholder="{{trans('setting.address',[''],$locale)}}" required>
                                @if($errors->has('address'))
                                <span class="help-block">{{$errors->first('address')}}</span>
                                @endif
                            </div>
                    
                    </div>
                        @endforeach
                            
                           
                        
                
                    
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.email')}}</label>
                                <input name="email" value="{{$setting->email}}" type="email" class="form-control" placeholder="{{trans('setting.email')}}" required>
                                @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                           
                              <div class="form-group">
                                <label class="control-label">{{trans('setting.phone')}}</label>
                                <input name="phone" value="{{$setting->phone}}" type="text" class="form-control" placeholder="{{trans('setting.phone')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                           
                             <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_number')}}</label>
                                <input name="whatsapp_number" value="{{$setting->whatsapp_number}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_number')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                            
                           <div class="form-group">
                                <label class="control-label">{{trans('setting.fax')}}</label>
                                <input name="fax" value="{{$setting->fax}}" type="text" class="form-control" placeholder="{{trans('setting.fax')}}" required>
                                @if($errors->has('fax'))
                                <span class="help-block">{{$errors->first('fax')}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.facebook_link')}}</label>
                                <input name="link_facebook" value="{{$setting->link_facebook}}" type="text" class="form-control" placeholder="{{trans('setting.facebook_link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.twitter_link')}} </label>
                                <input name="link_twitter" value="{{$setting->link_twitter}}" type="text" class="form-control" placeholder="{{trans('setting.twitter_link')}}" required>
                                @if($errors->has('twitter_link'))
                                <span class="help-block">{{$errors->first('twitter_link')}}</span>
                                @endif
                            </div>   
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_link')}} </label>
                                <input name="link_whatsapp" value="{{$setting->link_whatsapp}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_link')}}" required>
                                @if($errors->has('whatsapp'))
                                <span class="help-block">{{$errors->first('whatsapp')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                 <label class="control-label"> {{trans('setting.snapchat_link')}} </label>
                                <input name="link_snapchat" value="{{$setting->link_snapchat}}" type="text" class="form-control" placeholder="{{trans('setting.snapchat_link')}}" required>
                                @if($errors->has('youtube'))
                                <span class="help-block">{{$errors->first('youtube')}}</span>
                                @endif
                            </div>
                         
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.instagram_link')}}</label>
                                <input name="link_instagram" value="{{$setting->link_instagram}}" type="text" class="form-control" placeholder="{{trans('setting.instagram_link')}}" required>
                                @if($errors->has('instagram_link'))
                                <span class="help-block">{{$errors->first('instagram_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.linkend_link')}}</label>
                                <input name="link_linkedin" value="{{$setting->link_linkedin}}" type="text" class="form-control" placeholder="{{trans('setting.linkend_link')}}" required>
                                @if($errors->has('google_link'))
                                <span class="help-block">{{$errors->first('google_link')}}</span>
                                @endif
                            </div>
                          
                            
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getSettings')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form> 
                    @endif
                    @if ($setting == null){
                        <form method="post" action="{{route('addSettings')}}">
                        {{ csrf_field() }}
                       
                          <div class="form-body">
                                @include('backend.includes.check_language')
                                @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            
                                <div class="form-group">
                                <label class="control-label">{{trans('setting.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('setting.description',[''],$locale)}}" maxlength="100" required>{{old('description:'.$locale)}}</textarea>
                               
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('description:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.contactus_description',[''],$locale)}}</label>
                                <textarea name="contactus_description:{{$locale}}" class="form-control" placeholder="{{trans('setting.contactus_description',[''],$locale)}}" maxlength="100" required>{{old('contactus_description:'.$locale)}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.address',[''],$locale)}}</label>
                                <input name="address" value="{{old('address:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.address',[''],$locale)}}" required>
                                @if($errors->has('address'))
                                <span class="help-block">{{$errors->first('address')}}</span>
                                @endif
                            </div>
                    
                    </div>
                        @endforeach
                            
                           
                        
                
                    
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.email')}}</label>
                                <input name="email" value="{{old('email')}}" type="email" class="form-control" placeholder="{{trans('setting.email')}}" required>
                                @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                           
                              <div class="form-group">
                                <label class="control-label">{{trans('setting.phone')}}</label>
                                <input name="phone" value="{{old('phone:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.phone')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                           
                             <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_number')}}</label>
                                <input name="whatsapp_number" value="{{$setting->whatsapp_number}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_number')}}" required>
                                @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                            
                           <div class="form-group">
                                <label class="control-label">{{trans('setting.fax')}}</label>
                                <input name="fax" value="{{old('fax:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.fax')}}" required>
                                @if($errors->has('fax'))
                                <span class="help-block">{{$errors->first('fax')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.facebook_link')}}</label>
                                <input name="link_facebook" value="{{old('link_facebook:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.facebook_link')}}" required>
                                @if($errors->has('facebook_link'))
                                <span class="help-block">{{$errors->first('facebook_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.twitter_link')}} </label>
                                <input name="link_twitter" value="{{old('link_twitter:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.twitter_link')}}" required>
                                @if($errors->has('twitter_link'))
                                <span class="help-block">{{$errors->first('twitter_link')}}</span>
                                @endif
                            </div>   
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.whatsapp_link')}} </label>
                                <input name="link_whatsapp" value="{{old('link_whatsapp:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.whatsapp_link')}}" required>
                                @if($errors->has('whatsapp'))
                                <span class="help-block">{{$errors->first('whatsapp')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                 <label class="control-label"> {{trans('setting.snapchat_link')}} </label>
                                <input name="link_snapchat" value="{{old('link_snapchat:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.snapchat_link')}}" required>
                                @if($errors->has('youtube'))
                                <span class="help-block">{{$errors->first('youtube')}}</span>
                                @endif
                            </div>
                         
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.instagram_link')}}</label>
                                <input name="link_instagram" value="{{old('link_instagram:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.instagram_link')}}" required>
                                @if($errors->has('instagram_link'))
                                <span class="help-block">{{$errors->first('instagram_link')}}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">{{trans('setting.linkend_link')}}</label>
                                <input name="link_linkedin" value="{{old('link_linkedin:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('setting.linkend_link')}}" required>
                                @if($errors->has('google_link'))
                                <span class="help-block">{{$errors->first('google_link')}}</span>
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getSettings')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    }
                        
                    @endif
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
 
 @endsection