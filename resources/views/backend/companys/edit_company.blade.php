@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.update_company')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('updatecompanyById',['companyId'=>$company->id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                               @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('company.name',[''],$locale)}}</label>
                                <input name="name:{{$locale}}" value="{{ $company->translate($locale)->name}}" type="text" class="form-control" placeholder="{{trans('company.name',[''],$locale)}}" required>
                                @if($errors->has('name:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('company.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{$company->translate($locale)->title}}" type="text" class="form-control" placeholder="{{trans('company.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('company.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('company.description',[''],$locale)}}"  required>{{$company->translate($locale)->description}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                            
                         
                    </div>
                        @endforeach
                            
        
                          <div class="form-group">
                                <label class="control-label">{{trans('company.image')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="image" type="file" class="form-control" >
                                    <img height="100" src="{{ASSETS}}/images/companys/{{$company->image}}">
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label">{{trans('company.logo')}}</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-picture-o"></i>
                                    </span>
                                    <input name="logo" type="file" class="form-control" >
                                    <img height="100" src="{{ASSETS}}/images/companys/{{$company->logo}}">
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('image')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label">{{trans('company.path')}}</label>
                                    <div class="input-group">
                                       
                                        <input name="path" type="text" class="form-control" value="{{$company->path}}" required>
                                      
                                    </div>
                                </div>
                                <div class="from-group">
                           <label class="control-label">{{trans('company.direction')}}</label>
                          <select name="direction_id" class="form-control" required>
                                        <option value="">--{{trans('company.Selectdirection')}}--</option>
                                        @foreach ($direction as $item )
                                        <option value="{{$item->id }}"{{ $selecteddirection ==$item->id? 'selected="selected"' : '' }} >{{ $item->name }}</option>   
                                        @endforeach
                                    </select>
                          
                          </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllcompany')}}">{{trans('backend.cancel')}}</a>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
     
   
@endsection