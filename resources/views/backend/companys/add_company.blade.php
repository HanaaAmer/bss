@extends('backend.layouts.main_layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>{{trans('backend.add_company')}}</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="javascript:;" class="reload"> </a>
                        <a href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('postAddcompany')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                             @include('backend.includes.check_language')
                        @foreach (\Config::get('languages') as $locale=>$language) 
                        <div id="tap_{{$locale}}">
                            <div class="form-group">
                                <label class="control-label">{{trans('company.name',[''],$locale)}}</label>
                                <input name="name:{{$locale}}" value="{{old('name:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('company.name',[''],$locale)}}" required>
                                @if($errors->has('name:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('name:'.$locale)}}</span>
                                @endif
                            </div>
                                <div class="form-group">
                                <label class="control-label">{{trans('company.title',[''],$locale)}}</label>
                                <input name="title:{{$locale}}" value="{{old('title:'.$locale)}}" type="text" class="form-control" placeholder="{{trans('company.title',[''],$locale)}}" required>
                                @if($errors->has('title:{{$locale}}'))
                                    <span class="help-block">{{$errors->first('title:'.$locale)}}</span>
                                @endif
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">{{trans('company.description',[''],$locale)}}</label>
                                <textarea name="description:{{$locale}}" class="form-control" placeholder="{{trans('company.description',[''],$locale)}}"  required>{{old('description:'.$locale)}}</textarea>
                                @if($errors->has('description:{{$locale}}'))
                                    <span class="help-block">
                                    {{$errors->first('description'.$locale)}}
                                    </span>
                                @endif
                            </div>
                           
                        
                    </div>
                        @endforeach
                            
                          <div class="form-group">
                                    <label class="control-label">{{trans('company.image')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                        <input name="image" type="file" class="form-control" required>
                                        @if($errors->has('image'))
                                            <span class="help-block">{{$errors->first('image')}}</span>
                                        @endif
                                    </div>
                                </div>
                                 
                          
                                <div class="form-group">
                                    <label class="control-label">{{trans('company.logo')}}</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                        <input name="logo" type="file" class="form-control" required>
                                        @if($errors->has('image'))
                                            <span class="help-block">{{$errors->first('image')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{trans('company.path')}}</label>
                                    <div class="input-group">
                                       
                                        <input name="path" type="text" class="form-control" value="{{old('path')}}" required>
                                      
                                    </div>
                                </div>
                                <div class="from-group">
                           <label class="control-label">{{trans('company.direction')}}</label>
                         
                           
                          <select name="direction_id" class="form-control" required>
                                        <option value="">--{{trans('company.Selectdirection')}}--</option>
                                        
                                        @foreach ($direction as $item)
                                        <option value="{{$item->id }}"> {{$item->name}}</option>     
                                        @endforeach
                                         
                                    </select>
                                     </div>
                          </div>
                        <div class="form-actions">
                            <div class="btn-set pull-left">
                                <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                <a class="btn btn-danger" href="{{route('getAllcompany')}}">{{trans('backend.cancel')}}</a>
                            </div>
                        </div>
                         </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>

  
@endsection