<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
        <!-- Required Meta Tags -->
        @include('front.includes.meta')
        <link rel="shortcut icon" type="image/png"  href="{{ASSETS}}/front/img/logo.png">
        <!-- Required CSS Files -->
        @include('front.includes.css')
      
    </head>
    <body id="top">
        <!-- Important Headline in Home Only -->
        <h1 class="hidden">BSS Group | مجموعه بي اس اس</h1>

        <!-- Home Header -->
      
            <!-- Main Header -->
            <header class="tornado-header main-header wow" data-sticky>
            @include('front.includes.header')
            </header>
            <!-- // Main Header -->

          
        <!-- // Home Header -->
        @yield('content')
        @include('front.includes.footer')
        <!-- Main Footer -->
      

        <!-- Required JS Files -->
        <script src="{{ASSETS}}/front/js/jquery-3.3.1.min.js"></script>
        <script src="{{ASSETS}}/front/js/tornado.min.js"></script>
    </body>
</html>