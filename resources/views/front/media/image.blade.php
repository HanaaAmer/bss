@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
    <div class="row">
        <!-- Banner Block -->
        @include('front.includes.banar')
        <!-- Content Side -->
        <div class="col-12 col-m-8">
            <!-- Media Grid -->
            <div class="row">
                <!-- Media Block -->
                @foreach ($images as $imag)
                <div class="media-block col-12 col-m-6">
                <div class="content-box">
                        @foreach($imag->image as $index=>$its)
                        @if ($index == 0)
                    <a href="{{route('getSelectedGallary',['id'=>$imag->id])}}" class="image ti-wallpaper" data-src="{{UPLOADS}}/images/{{$its}}"></a>
                    @endif
                    @endforeach   
                    <a href="{{route('getSelectedGallary',['id'=>$imag->id])}}"><h3>{{$imag->album_title}}</h3></a>
                </div>
            </div>
                @endforeach
               
            </div>
           
        </div>
        @include('front.includes.Widgetspages')
    </div>
</div>
@endsection