@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
    <div class="row">
        <!-- Banner Block -->
        @include('front.includes.banar')
        <!-- Content Side -->
                <div class="col-12 col-m-8">
                        <div class="content-wrap">
                            <h2 class="title">{{$images->album_title}}</h2>
                            <!-- Images Slider -->
                            <div class="photo-slider">          
                               @foreach($images->image as $index=>$its)                   
                                 <a href="{{UPLOADS}}/images/{{$its}}" class="item" data-src="{{UPLOADS}}/images/{{$its}}" target="_blank"></a>                                     
                                    @endforeach 
                            </div>
                            <!-- Images Thumbnails -->
                            <div class="photo-thumbnails row">
                                     @foreach($images->image as $index=>$its)              
                                    <a href="javascript:void(0);" class="item" data-src="{{UPLOADS}}/images/{{$its}}"></a>                                  
                                    @endforeach
                            </div>
                            <!-- Rich Content -->
                            <div class="rich-content">{!! html_entity_decode( $images->long_description) !!}</div>
                                
                            <div class="responsive-video">
                                
                            </div>
                            <!-- Sharebar -->
                            <div class="sharebar">
                                <a href="#" class="btn primary rounded outline" data-modal="comment-form">أضافه تعليق</a>
                                <div class="float-end">
                                <a  class="btn dark rounded outline">مشاركة المقال</a>
                                <a href="http://www.facebook.com/sharer.php?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-facebook"></a>
                                <a href="https://twitter.com/share?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-twitter"></a>
                                <a  href="https://www.instagram.com/?url{{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-instagram"></a>
                                </div>
                            </div>
                            <ul class="comments">
                                <!-- Comment Block -->
                                @foreach ($ImagesComments as $ImagesComment)
                                <li class="comment-block">
                                    <img src="https://via.placeholder.com/50x50" alt="">
                                    <h3 class="title"><span>تعليق بواسطة</span> {{$ImagesComment->name}}</h3>
                                    <p>{{$ImagesComment->comment}}</p>
                                </li>
                              @endforeach 
                            </ul>
                                    
                <div class="modal-box" id="comment-form">
                        <!-- Container -->
                        <div class="modal-content">
                            <!-- Headline -->
                            <h2 class="modal-head">
                                اترك تعليقا
                                <button class="close-modal ti-close"></button>
                            </h2>
                            <!-- Content -->
                            <div class="modal-body">
                                 <form  class="form-ui" method="post" action="{{route('postAddImagesComments')}}" enctype="multipart/form-data">
                                     {{ csrf_field() }}       
                                     <input type="hidden" name="image_id" value="{{$images->id}}" />
                                    <label>اسمك</label>
                                    <input name="name" type="text" placeholder="اكتب اسمك بالكامل">
                                    <label>البريد الالكتروني</label>
                                    <input name="email" type="text" placeholder="اكتب بريدك الاكتروني">
                                    <label>التعليق</label>
                                    <textarea  name="comment" placeholder="اكتب تعليقك"></textarea>
                                    <input type="hidden" name="active" value="0" />
                                    <input type="submit" value="اضافه التعليق" class="btn primary">
                                </form>
                            </div>
                        </div>
                        <!--// Container -->
                    </div>
                        </div>
                    
                </div>
                @include('front.includes.Widgetspages')  
    </div>
</div>

@endsection