@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
    <div class="row">
        <!-- Banner Block -->
        @include('front.includes.banar')
        <div class="col-12 col-m-8">
                <div class="content-wrap">
                    <h2 class="title">اخر الاخبار</h2>
                    <!-- News Grid -->
                    <div class="row">
                        <!-- News Block -->
                        @foreach ($News as $new)
                        <div class="news-block col-12 col-m-6">
                        <a href="{{route('getSelectedNews',['id'=>$new->id])}}" class="image ti-wallpaper" data-src="{{UPLOADS}}/News/{{$new->titleImage}}"></a> 
                            <div class="info">
                                <a href="{{route('getSelectedNews',['id'=>$new->id])}}"><h3>{{$new->title}}</h3></a>
                                <p>{{$new->short_description}}</p>
                            </div>
                        </div>
                @endforeach
                    </div>
            </div>
        </div>
        @include('front.includes.Widgetspages')
    </div>
</div>
@endsection