@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
    <div class="row">
        <!-- Banner Block -->
        @include('front.includes.banar')
        <!-- Content Side -->
                <div class="col-12 col-m-8">
                        <div class="content-wrap">
                            <h2 class="title">{{$News->title}}</h2>
                            <img src="{{UPLOADS}}/News/{{$News->image}}" class="cover block-lvl" alt="عنوان الخبر الاساسي">
                        <!-- Rich Content -->
                        <div class="rich-content">
                            <p>{!! html_entity_decode( $News->long_description) !!} </p>
                        </div>
                            <!-- Sharebar -->
                            <div class="sharebar">
                                <a href="#" class="btn primary rounded outline" data-modal="comment-form">أضافه تعليق</a>
                                <div class="float-end">
                                <a  class="btn dark rounded outline">مشاركة المقال</a>
                                <a href="http://www.facebook.com/sharer.php?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-facebook"></a>
                                <a href="https://twitter.com/share?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-twitter"></a>
                                <a  href="https://www.instagram.com/?url{{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-instagram"></a>
                                </div>
                            </div>
                            <ul class="comments">
                                <!-- Comment Block -->
                                @foreach ($NewsComments as $NewsComment)
                                <li class="comment-block">
                                    <img src="https://via.placeholder.com/50x50" alt="">
                                    <h3 class="title"><span>تعليق بواسطة</span> {{$NewsComment->name}}</h3>
                                    <p>{{$NewsComment->comment}}</p>
                                </li>
                              @endforeach 
                            </ul>
                       
                <div class="modal-box" id="comment-form">
                    <!-- Container -->
                    <div class="modal-content">
                        <!-- Headline -->
                        <h2 class="modal-head">
                            اترك تعليقا
                            <button class="close-modal ti-close"></button>
                        </h2>
                        <!-- Content -->
                        <div class="modal-body">
                             <form  class="form-ui" method="post" action="{{route('postAddNewsComments')}}" enctype="multipart/form-data">
                                 {{ csrf_field() }}       
                                 <input type="hidden" name="news_id" value="{{$News->id}}" />
                                <label>اسمك</label>
                                <input name="name" type="text" placeholder="اكتب اسمك بالكامل">
                                <label>البريد الالكتروني</label>
                                <input name="email" type="text" placeholder="اكتب بريدك الاكتروني">
                                <label>التعليق</label>
                                <textarea  name="comment" placeholder="اكتب تعليقك"></textarea>
                                <input type="hidden" name="active" value="0" />
                                <input type="submit" value="اضافه التعليق" class="btn primary">
                            </form>
                        </div>
                    </div>
                    <!--// Container -->
                </div>
            </div>
                    
        </div>
        @include('front.includes.Widgetspages')
    </div>
</div>

@endsection