<?php $welcome= \App\Repositories\companysRepository::getwelcome(); ?>
<div class="welcome-section container wow">
                <div class="row align-center-y">
                    <div class="col-12 col-m-6 col-l-7">
                    <h3>{{$welcome->short_description}}</h3>
                    {!! html_entity_decode($welcome->long_description) !!}
                    </div>
                    <div class="hidden-s-down col-m-6 col-l-5">
                        <img src="{{ASSETS}}/images/pages/{{$welcome->image}}" alt="">
                    </div>
                </div>
            </div>
          