<?php $Jobs= \App\Repositories\JobRepository::getlatestjob(); 
$News= \App\Repositories\NewsRepository::getlatestNews();
$Images = \App\Repositories\ImageRepository::getlatestImages();
?>
<!-- Widgets Side -->
<div class="col-12 col-m-4">
                    <div class="widget-wrap">
                        <!-- Jobs -->
                        <h2 class="title">الوظائف المتاحة</h2>
                        <ul class="content-list">
                            <!-- Item -->
                            @foreach($Jobs as $item)
                            <li class="content-item">
                                <a href="{{route('jobdetails',['jobId'=>$item->id])}}"><img src="{{ASSETS}}/images/jobs/{{$item->image}}" alt=""></a>
                                <div class="info">
                                    <a href="{{route('jobdetails',['jobId'=>$item->id])}}"><h3>{{$item->name}}</h3></a>
                                    <p>{{$item->created_at->todatestring()}}</p>
                                </div>
                            </li>
                            @endforeach
                          
                           
                        </ul>
                        <!-- Activities -->
                        <h2 class="title">اخر النشاطات</h2>
                        <ul class="content-list">
                        @foreach($News as $item)
                            <li class="content-item">
                                <a href="{{route('getSelectedNews',['id'=>$item->id])}}"><img src="{{UPLOADS}}/News/{{$item->image}}" alt=""></a>
                                <div class="info">
                                    <a href="{{route('getSelectedNews',['id'=>$item->id])}}"><h3>{{$item->title}}</h3></a>
                                    <p>{{$item->created_at->todatestring()}}</p>
                                </div>
                            </li>
                            @endforeach
                          
                        </ul>
                        <!-- Gallery -->
                        <h2 class="title">ألبوم الصور</h2>
                        <ul class="content-list">
                        @foreach($Images as $item)
                            <li class="content-item">
                            @foreach($item->image as $index=>$its)
                        @if ($index == 0)
                        <a href="{{route('getSelectedGallary',['id'=>$item->id])}}"><img src="{{UPLOADS}}/images/{{$its}}" alt=""></a>
             
                    @endif
                    @endforeach   
                               
                                <div class="info">
                                    <a href="{{route('getSelectedGallary',['id'=>$item->id])}}"><h3>{{$item->album_title}}</h3></a>
                                    <p>{{$item->created_at->todatestring()}}</p>
                                </div>
                            </li>
                            @endforeach
                          
                        </ul>
                    </div>
                </div>