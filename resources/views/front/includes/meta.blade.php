@php $links=\App\Repositories\LinkRepository::getAllFrontLinks()->where('active',1);
            $route=Route::currentRouteName();
            $link=\App\Repositories\LinkRepository::getAllFrontLinks()->where('route',$route)->first();
            if($link!=null){
            $meta=\App\Repositories\MetaRepository::getMeta($link->link_id);} 
            @endphp
        <!-- Required Meta Tags -->
        @if($meta!=null){
        <meta name="language" content="ar">
        <meta http-equiv="x-ua-compatible" content="text/html" charset="utf-8">
        <meta name="title" content="{{$meta->title}}" />
        <meta name="description" content="{{$meta->description}}" />
        <meta name="keywords" content="{{$meta->keywords}}" />
        <title>BSS Group</title>
        <!-- Open Graph Meta Tags -->
        <meta property="og:title" content="{{$meta->title}}" />
        <meta property="og:description" content="{{$meta->description}}" />
        <meta property="og:url" content="http://domain.com/page-url/" />
        <meta property="og:image" content="{{ASSETS}}/front/img/logo.png" />
        <!-- Twitter Card Meta Tags -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="{{$meta->title}}">
        <meta name="twitter:description" content="{{$meta->description}}">
        <meta name="twitter:image" content="{{ASSETS}}/front/img/logo.png">
        <!-- Facebook Card Meta Tags -->
        <meta name="facebook:image" content="{{ASSETS}}/front/img/logo.png"/>
        <meta name="facebook:title" content="{{$meta->title}}" />
        <meta name="facebook:description" content="{{$meta->description}}" />
        @endif
        <!-- Other Meta Tags -->
        <meta name="robots" content="index, follow" />
        <meta name="copyright" content="Sitename Goes Here">
        
