<?php $companys= \App\Repositories\companysRepository::getcompanys(); 
$subscrib= \App\Repositories\companysRepository::getsubscrib();
$contactus = \App\Repositories\SettingRepository::getAllLINK();
?>
 
 <!-- Floating Elements -->
 <div class="newsletter-cta wow">
            <div class="container">
                <div class="row align-center-y">
                    <div class="col-12 col-m-6 col-l-7">
                    <h3>{{$subscrib->short_description}}</h3>
                    {!! html_entity_decode($subscrib->long_description) !!}
                    </div>
                    <div class="col-12 col-m-6 col-l-5">
                        <form method="post" action="{{route('postAddContactus')}}"  class="form-ui newsletter-form">
                          {{ csrf_field() }}
                            <input type="email" name="email" placeholder="اكتب بريدك الالكتروني هنا">
                            <input type="hidden" name="contact_from"  value="newsletter">
                            <input type="submit"   value="اشتراك" class="btn warning">
                        </form>
                    </div>
                </div>
            </div>
        </div>
  <div class="floating-companies">
            <!-- Logo Block -->
            @foreach($companys as $company)
            <a href="{{$company->path}}" class="mashroo3k"><img src="{{ASSETS}}/images/companys/{{$company->logo}}" alt=""></a>
            @endforeach
        </div>
        <a href="#top" class="scroll-up ti-arrow-up-c"></a>
<footer class="main-footer wow">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-m-8">
                        <h3>خريطة الموقع</h3>
                        <ul class="links">
                        @php $links=\App\Repositories\LinkRepository::getAllFrontLinks()->where('active',1) @endphp
                        @foreach($links as $i=>$link)
                        @if(Route::has($link->route))
                        <li><a href="{{route($link->route)}}">{{$link->title}}</a></li>
                        @else
                        <li><a href="{{$link->route}}">{{$link->title}}</a></li>
                        @endif
                        @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-m-4">
                        <h3>تواصل معنا</h3>
                        <ul class="contact-list">
                            <li class="ti-mail">{{$contactus->email}}</li>
                            <li class="ti-phone-in-talk">{{$contactus->phone}}</li>
                            <li class="ti-map-marker-multiple">{{$contactus->address}}</li>
                        </ul>
                        <div class="btns">
                            <a href="{{route('getjobs')}}" class="btn primary outline small rounded">وظائف متاحة</a>
                            <a href="{{$contactus->link_facebook}}" class="icon-btn ti-facebook"></a>
                            <a href="{{$contactus->link_twitter}}" class="icon-btn ti-twitter"></a>
                            <a href="{{$contactus->link_instagram}}" class="icon-btn ti-instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyrights wow">
            <div class="container">
                <p>صنع بكل حـــــ<span class="ti-heart"></span>ـــب فى مختبر</p>
                <a href="#" class="mahacode"><img src="{{ASSETS}}/front/img/mahacode-2.png" alt=""></a>
            </div>
        </div>