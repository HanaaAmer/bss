<?php 
$contactus = \App\Repositories\SettingRepository::getAllLINK();
?>
      <div class="container">
                    <!-- Logo -->
                    <a href="{{route('getFrontHome')}}" class="logo"><img src="{{ASSETS}}/front/img/logo.png" alt=""></a>
                    <!-- Navigation Menu -->
                    <div class="navigation-menu" data-id="main-menu" data-logo="img/logo.png">
                        <ul>
                        @php $links=\App\Repositories\LinkRepository::getAllFrontLinks()->where('active',1) @endphp
                        @foreach($links as $i=>$link)
                        @if(Route::has($link->route))
                        <li><a href="{{route($link->route)}}">{{$link->title}}</a></li>
                        @else
                        <li><a href="{{$link->route}}">{{$link->title}}</a></li>
                        @endif
                        @endforeach
                        </ul>
                    </div>
                    <!-- Action Buttons -->
                    <div class="action-btns">
                        <a href="{{route('getjobs')}}" class="btn primary outline small rounded">وظائف متاحة</a>
                        <a href="#" class="icon-btn ti-menu-round menu-btn" data-id="main-menu"></a>
                        <a href="{{$contactus->link_facebook}}" class="icon-btn ti-facebook"></a>
                            <a href="{{$contactus->link_twitter}}" class="icon-btn ti-twitter"></a>
                            <a href="{{$contactus->link_instagram}}" class="icon-btn ti-instagram"></a>
                    </div>
                </div>