@extends('front.layouts.front_layout')
@section('meta')
    <title>{{ \App\Http\Controllers\HomeController::translateWord('about_us')}} </title>
@endsection

@section('content')
        <!-- Page Head -->
        <div class="page-head" data-src="{{ASSETS}}/front/img/about-head.png">
            <div class="container tx-align-center">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('about_us') }}</h1>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- About US -->
        <div class="about-us">
            <div class="container">
                <div class="row align-center-y">
                    <!-- Image -->
                    <div class="col-12 col-m-6"> <img src="{{UPLOADS}}/pages/{{$page->image}}" alt="about gessor" class="block-lvl"> </div>
                    <!-- Content -->
                    <div class="col-12 col-m-6">
                        <span>{{ \App\Http\Controllers\HomeController::translateWord('about_us') }}</span>
                       <p> {!!html_entity_decode($page->long_description)!!} </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- // About US -->

        <!-- Our Partners -->
        <div class="who">
            <div class="container">
                <h2 class="section-title">{{ \App\Http\Controllers\HomeController::translateWord('our_partners') }}</h2>
                <!-- Grid -->
                @php
                $sponsors=\App\Http\Controllers\HomeController::getFrontSponsors();
                @endphp
                <div class="company-grid">
                    @foreach($sponsors as $i=>$sponsor)
                    <a href="#company-profile" data-title="{{$sponsor->name}}"><img src="{{UPLOADS}}/sponsors/{{$sponsor->image}}" alt="Company Title"></a>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
        <!-- // Our Partners -->

        <!-- Our Team -->
        <div class="teamwork">
            <div class="container">
                <h2 class="section-title">{{ \App\Http\Controllers\HomeController::translateWord('meet_our_team') }}</h2>
                <!-- Team Slider -->
                <div class="team-slider row">
                @php
                $persons=\App\Http\Controllers\HomeController::getFrontPersons();
                @endphp
                @foreach($persons as $index=>$person)
                    <!-- Team Block -->
                    <div class="team-block box-5x1">
                        <i><img src="{{UPLOADS}}/persons/{{$person->image}}" alt=""></i>
                        <h3>{{$person->name}}</h3>
                        <h4>{{$person->specialist_in}}</h4>
                    </div>
                @endforeach   
                </div>
            </div>
        </div>
        <!-- // Our Team -->

        <!-- Statistics -->
        @include('front.includes.statistics')
        <!-- // Statistics -->
@endsection