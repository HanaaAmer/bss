@extends('front.layouts.front_layout')
@section('meta')
    <title>{{ \App\Http\Controllers\HomeController::translateWord('contactus')}}</title>
@endsection

@section('content')
<!-- Page Head -->
<div class="page-head" data-src="{{ASSETS}}/front/img/head.png">
            <div class="container tx-align-center">
                <h1>{{ \App\Http\Controllers\HomeController::translateWord('contactus')}}</h1>
            </div>
        </div>
        <!-- // Page Head -->

        <!-- Page Content -->
        <div class="container page-content">
            <!-- Page Grid -->
            <div class="row">
                <!-- Info Side -->
                <div class="col-12 col-m-4">
                    <h2 class="area-title">{{ \App\Http\Controllers\HomeController::translateWord('contact_info')}}</h2>
                    <ul class="contact-info">
                        <li class="ti-phone"><a href="#">+{{\App\Repositories\SettingRepository::getSettingValue('phone')}}</a></li>
                        <li class="ti-mail"><a href="#">{{\App\Repositories\SettingRepository::getSettingValue('email_1')}}</a></li>
                        <li class="ti-earth"><a href="http://www.eltawfikjobs.com/">www.eltawfikjobs.com</a></li>
                        <li class="ti-map-marker"><a href="#">{{\App\Repositories\SettingRepository::getSettingValue('address_'.App::getLocale())}}</a></li>
                    </ul>
                </div>
                <!-- Form Side -->
                <div class="col-12 col-m-8">
                    <h2 class="area-title">{{ \App\Http\Controllers\HomeController::translateWord('send_message')}}</h2>
                    <form method="post" action="{{route('postAddContactus')}}" class="form-ui contact-form row">
                        {{ csrf_field() }}
                        <!-- Form Control -->
                        <div class="col-12 col-m-4"> <input name="name" type="text" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('contact_name')}}" required> </div>
                        <!-- Form Control -->
                        <div class="col-12 col-m-4"> <input name="email" type="email" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('contact_email')}}"> </div>
                        <!-- Form Control -->
                        <div class="col-12 col-m-4"> <input name="phone" type="text" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('contact_phone')}}"> </div>
                        <!-- Form Control -->
                        <div class="col-12">
                        <textarea name="message" placeholder="{{ \App\Http\Controllers\HomeController::translateWord('contact_message')}}" required></textarea>
                        <input name="contact_from" type="hidden" value="contactus">   
                        <input type="submit" value="{{ \App\Http\Controllers\HomeController::translateWord('send_message')}}" class="btn primary">
                        </div>
                    </form>
                </div>
            </div>
            <!-- Page Grid -->
        </div>
        <!-- // Page Content -->
@endsection


