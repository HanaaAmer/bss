@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
            <div class="row">
                <!-- Banner Block -->
                @include('front.includes.banar')
                <!-- Content Side -->
                <div class="col-12 col-m-8">
                    <!-- Search Bar -->
                    <div class="search-bar">
                      
                        <form method="GET" action="{{route('search')}}" class="form-ui search small">
                        {{csrf_field()}}
                            <input name="search_name" type="text" placeholder="بحث سريع">
                            <input type="submit" value="بحث" class="btn small primary">
                        </form>
                        <form class="form-ui small filter">
                            <label>اظهار</label>
                            <select class="chevron-select" onchange="document.location=this.options[this.selectedIndex].value">
                            @foreach($dropdown as $item)
                                <option name="name" value="{{route($item->route,['routId'=>$item->id])}}" {{ $selectedroute ==$item->id? 'selected="selected"' : '' }}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                    <!-- Jobs List -->
                    <div class="responsive-table">
                        <table class="table striped bordered">
                            <thead>
                                <tr>
                                    <th class="col-5">المسمي الوظيفي</th>
                                    <th class="col-3">اسم الشركة الموظفه</th>
                                    <th class="col-2">العدد المطلوب</th>
                                    <th class="col-2"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($Jobs as $Job)
                                <tr>
                                    <td class="col-5"> <a href="{{route('jobdetails',['jobId'=>$Job->jobs_id])}}">{{$Job->name}}</a> </td>
                                    <td class="col-3"> <a href="{{route('jobdetails',['jobId'=>$Job->jobs_id])}}" class="secondary-color">{{$Job->Jobs->companys->name}}</a> </td>
                                    <td class="col-2">{{$Job->requirednumber}}</td>
                                  
                                </tr>
                               @endforeach
                                <tr>
                                    <td class="col-5" colspan="4"> <span class="danger-color">الرجاء عدم نسيان اى من بيانات التقديم على الوظيفه</span> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- Pagination -->
                    <ul class="pagination">
                    {{$Jobs->links()}}
                    </ul>   
                  
                </div>
                @include('front.includes.Widgets')
            </div>
        </div>
@endsection