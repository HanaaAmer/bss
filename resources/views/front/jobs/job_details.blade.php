@extends('front.layouts.front_layout')
@section('content')
<div class="container page-content">
            <div class="row">
                <!-- Banner Block -->
                @include('front.includes.banar')
                <!-- Content Side -->
                <div class="col-12 col-m-8">
                    <div class="content-wrap">
                        <h2 class="title">{{$Jobs->name}}</h2>
                        <!-- Cover Image -->
                        <img src="{{ASSETS}}/images/jobs/{{$Jobs->image}}" class="cover block-lvl" alt="عنوان الخبر الاساسي">
                        <!-- Rich Content -->
                        <div class="rich-content">
                            <h5>{{$Jobs->title}}</h5>
                            {!! html_entity_decode($Jobs->description) !!}
                        </div>
                        <!-- Requerments -->
                        <h3 class="requerments-title">متطلبات الوظيفه</h3>
                        <div class="row job-requerments">
                            <div class="col-12 col-m-6">
                                <ul>
                                    @foreach($Jobs->requirements as $req)
                                    <li>{{$req}}</li>
                                    @endforeach
                                 
                                </ul>
                            </div>
                           
                        </div>
                        <!-- Apply -->
                        <a href="#" data-modal="job-apply" class="btn primary block-lvl apply-button">التقدم للوظيفه</a>
                       
    <div class="modal-box" id="job-apply">
                            <!-- Container -->
                            <div class="modal-content">
                                <!-- Headline -->
                                <h2 class="modal-head">
                                    التقدم للوظيفه
                                    <button class="close-modal ti-close"></button>
                                </h2>
                                <!-- Content -->
                                <div class="modal-body">
                                <form class="form-ui" method="post" action="{{route('postAddprogressofthejob')}}" enctype="multipart/form-data">
                                 {{ csrf_field() }}
                                 <input  name="jobs_id" type="hidden" value="{{$Jobs->id}}">
                                        <label>الاسم</label>
                                        <input name="name" type="text" placeholder="الاسم بالكامل"  required>
                                        <label>البريد الالكتروني</label>
                                        <input name="email" type="email" placeholder="البريد الاكتروني"  required> 
                                        <label>رقم الهاتف</label>
                                        <input name="phone" type="text" placeholder="رقم الهاتف"  required>
                                        <label>رفع السيرة الذاتيه</label>
                                        <div class="file-input" data-text="رفع ملف" data-btn="رفع">
                                            <input name="cv" type="file"  required>
                                        </div>
                                        <button type="submit" class="btn primary block-lvl">أرسال البيانات</button>
                                       
                                    </form>
                                </div>
                                <!-- Footer -->
                            </div>
                            <!--// Container -->
                        </div>

                        <!-- Sharebar -->
                        <div class="sharebar">
                            <div class="float-end">
                                <a  class="btn dark rounded outline">مشاركة المقال</a>
                                <a href="http://www.facebook.com/sharer.php?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-facebook"></a>
                                <a href="https://twitter.com/share?url={{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-twitter"></a>
                                <a  href="https://www.instagram.com/?url{{ urlencode(Request::fullUrl()) }}" class="icon-btn ti-instagram"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Widgets Side -->
                @include('front.includes.Widgetspages')
            </div>
        </div>
        @endsection