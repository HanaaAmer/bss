@extends('front.layouts.bssfront_layouthome')
@section('content')
        <!-- Companies Group -->
        <div class="companies-group wow">
            <div class="container">
                <div class="row">
                    <!-- Logo Block -->
                    @foreach($companys as $company)
                    <div class="col-6 col-m-4 col-l-2 logo-block">
                        <a href="{{$company->path}}" class="mashroo3k"><img src="{{ASSETS}}/images/companys/{{$company->logo}}" alt=""></a>
                    </div>
                    @endforeach
                    <!-- Logo Block -->
                   
                </div>
            </div>
        </div>
        <!-- // Companies Group -->

        <!-- Environment -->
        <div class="section environment-section wow">
            <div class="container">
                <!-- Content -->
                <div class="content">
                    <h3>{{$item->short_description}}</h3>
                    {!! html_entity_decode($item->long_description) !!}
                </div>
                <!-- Vector Image -->
                <div class="vector-image hidden-m-down">
                    <img src="{{ASSETS}}/images/pages/{{$item->image}}" alt="">
                </div>
            </div>
        </div>
        <!-- // Environment -->

        <!-- Company Section -->
        @foreach($companys as $company)
        @if($company->direction_id==2)
        <div class="company-section mashroo3k wow">
            <div class="container">
                <div class="row align-center-y">
                    <div class="hidden-s-down col-m-5">
                        <!-- Vector Image -->
                        <img src="{{ASSETS}}/images/companys/{{$company->image}}" alt="" class="vector-image">
                    </div>
                    <div class="col-12 col-m-7">
                        <!-- Content -->
                        <div class="content-box">
                            <img src="{{ASSETS}}/images/companys/{{$company->logo}}" alt="">
                            <h2>{{$company->title}}</h2>
                            <p>{{$company->description}}</p>
                            <div class="btns">
                            <a href="{{$company->path}}" class="btn mahacode outline rounded">زيارة {{$company->name}}</a>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Company Section -->
        @endif
        <!-- Company Section -->
        @if($company->direction_id==3)
        <div class="company-section mahacode wow">
            <div class="container">
                <div class="row row-reverse align-center-y">
                    <div class="hidden-s-down col-m-5">
                        <!-- Vector Image -->
                        <img src="{{ASSETS}}/images/companys/{{$company->image}}" alt="" class="vector-image">
                    </div>
                    <div class="col-12  col-m-7">
                        <!-- Content -->
                        <div class="content-box">
                            <img src="{{ASSETS}}/images/companys/{{$company->logo}}" alt="">
                            <h2>{{$company->title}}</h2>
                            <p>{{$company->description}}</p>
                            <div class="btns">
                                <a href="{{$company->path}}" class="btn mahacode outline rounded">زيارة {{$company->name}}</a>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <!-- // Company Section -->
        @endforeach
      
        <!-- Newsletter -->
        

@endsection