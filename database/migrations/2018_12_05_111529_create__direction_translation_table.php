<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectionTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direction_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->nullable();
            $table->string('name')->nullable();
            $table->integer('direction_id')->nullable()->unsigned();
            $table->foreign('direction_id')->references('id')->on('direction')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direction_translation');
    }
}
