<?php
require 'define.php';
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
Route::get('/403',function(){ return view('errors.403');})->name('unauthorised');
Route::group(['prefix' => 'admin'],function ()
{
    Auth::routes();
});

Route::group(['prefix' => 'admin', 'middleware'=>['auth','laguageChooser']],function ()
{
    Route::get('/',function(){ 
        if(Auth::user()->role_id == 2 || Auth::user()->role_id == 3){
            return redirect('/');
        }
        return view('backend.layouts.statistics_layout');
    })->name('getBackendHome');

    Route::group(['prefix'=>'contacts'],function (){
        // Links
        Route::get('/contactus',['uses'=>'ContactController@index','as'=>'getAllContacts']);
        Route::get('/contact/{contactId}',['uses'=>'ContactController@show','as'=>'getContactById']);
        Route::post('/contact/{contactId}',['uses'=>'ContactController@delete','as'=>'deleteContactById']);
    });
});

Route::get('/search', ['uses'=>'SearchController@search','as'=>'search']);


