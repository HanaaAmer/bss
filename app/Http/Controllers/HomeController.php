<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SliderRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SponsorsRepository;
use App\Repositories\BlogRepository;
use App\Entities\BlogCategory as BlogCategoryEntity;
use App\Entities\Page;
use App\Entities\Blog;

use App\Entities\Comment;
use App\Entities\Sponsor;
use App\Entities\Client;
use App\Entities\Translation;
use Lang;

use Modules\Frontend\Media\Models\Image as ImageModel;
use Modules\Frontend\Media\Models\Video as VideoModel;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    // protected $sliderRepository;
    // protected $serviceRepository;
    // protected $sponsorRepository;
    // protected $blogRepository;


    public function __construct()
    {
        // $this->sliderRepository  = new SliderRepository();
        // $this->serviceRepository = new ServiceRepository();
        // $this->sponsorRepository = new SponsorsRepository();
        // $this->blogRepository = new BlogRepository();
    }

    static public function translateWord($word)
    {
        if(is_object(Translation::where('word',$word)->where('lang',Lang::getLocale())->first()))
        {
            return Translation::where('word',$word)->where('lang',Lang::getLocale())->first()->translation;
        }        
            foreach (\Config::get('languages') as $locale=>$language) 
            {
                $translate=new Translation();
                $translate->word=$word;
                $translate->lang=$locale;
                $translate->translation=$word;
                $translate->save();
            }
            return Translation::where('word',$word)->where('lang',Lang::getLocale())->first()->translation;
    }
    
    static public function getAllBlogsToFooter(){
        $blogs=Blog::orderBy('created_at','desc')->take(3)->get();
        return $blogs;
    }
    static public function getAboutusFooter(){
        $aboutfooter=Page::find(1)->short_description;
        return $aboutfooter;
    }
    

}
