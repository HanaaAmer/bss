<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Entities\SidebarItems;
use App\Entities\UserRoles;
class RouteAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() && Request::is('admin/*') == Request::url() )
        {
            if(Auth::user()->role_id !=1)
            {
                return redirect()->route('getFrontHome'); 
            } 
            $items_access=unserialize(UserRoles::find(Auth::user()->role_id)->items_access);
            $all_items[]=SidebarItems::select('route')->get();
            $current_route=Request::route()->getName();
            if(!($request->isMethod('post') || $request->isMethod('put')))
            {
                if(!in_array ($current_route, $items_access))
                {
                    return redirect()->route('unauthorised');
                }
    
            }
        }
        return $next($request);
    }
}
