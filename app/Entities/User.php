<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'role_id', 'name', 'user_first_name', 'user_last_name', 'image', 'country', 'city', 'address', 'phone', 'education', 'experience', 'user_cv', 'skills_title','skills_degree', 'facebook_link', 'linkedin_link', 'twitter_link', 'google_link', 'company_name', 'bussiness_field', 'official_website', 'about_company', 'email', 'password', 'created_by', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
