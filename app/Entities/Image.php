<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use Translatable;
    public $translatedAttributes = ['album_title','long_description'];
    protected $fillable = [
        'album_title',
        'image',
        'long_description',
        ];
        protected $casts = [
            'image' => 'array',
        ];
    protected $table = "images";
}
