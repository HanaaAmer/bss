<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class NewsComments extends Model
{
    protected $fillable = [
        'news_id', 'name', 'email', 'comment'
    ];

    public function News() 
    {
        return $this->belongsTo(News::class);
    }

}
