<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;


class direction extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];
    protected $fillable=['id'];
    protected $primaryKey='id';
    protected $table='direction';
   
}
