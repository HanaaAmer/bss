<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{

    protected $fillable=['title', 'short_description','long_description'];
    protected $table='news_translation';
   
}