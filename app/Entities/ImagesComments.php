<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ImagesComments extends Model
{
    protected $fillable = [
        'image_id', 'name', 'email', 'comment'
    ];

    public function Image() 
    {
        return $this->belongsTo(Image::class);
    }

}
