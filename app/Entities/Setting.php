<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Translatable;
    public $translatedAttributes = ['contactus_description','description','address'];
    protected $fillable = [
        'email',
        'phone',
        'whatsapp_number',
        'fax',
        'map_lat',
        'map_lng',
        'link_facebook',
        'link_twitter',
        'link_snapchat',
        'link_instagram',
        'link_linkedin',
        'link_whatsapp',       

    ];
    protected $table='contactinfo';
    protected $primaryKey='id';
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("contactus_description", "LIKE","%$keyword%")
                      ->orWhere("description", "LIKE", "%$keyword%");
                
});            
        }
        return $query;
    }
}    