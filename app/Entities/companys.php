<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;


class companys extends Model
{
    use Translatable;
    public $translatedAttributes = ['name','title','description'];
    protected $fillable=['id','image','path','logo','direction_id'];
    protected $primaryKey='id';
    protected $table='companys';
    public function direction(){
        return $this->belongsTo(direction::class,'direction_id','id');
    }
   
}
