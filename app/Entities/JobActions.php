<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class JobActions extends Model
{
    protected $fillable=['id','job_id','user_id', 'company_id','saved','apply','image','how_to_apply'];
    protected $table='job_actions';
    protected $primaryKey='id';
    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id','id');
    }
    public function job()
    {
        return $this->belongsTo('App\Entities\Job', 'job_id','id');
    }
}
