<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class JobsTranslation extends Model
{
   protected $fillable= ['name','title', 'description','requirements','requirednumber','jobs_id'];
    protected $table='jobs_translation';
    protected $primaryKey='id';
    protected $casts = [
        'requirements' => 'array'
      ];
      public function Jobs()
      {
          return $this->belongsTo(Jobs::class,'jobs_id','id');
      }
}