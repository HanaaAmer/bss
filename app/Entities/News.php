<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use Translatable;
    public $translatedAttributes = ['title', 'short_description','long_description'];
    protected $fillable=['title', 'short_description','long_description','image', 'titleImage','active'];
    protected $table='news';
    protected $primaryKey='id';

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '')
        {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("short_description", "LIKE", "%$keyword%")
                    ->orWhere("long_description", "LIKE", "%$keyword%");
                   
            });
        }
        return $query;
    }
}
