<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class JobSections extends Model
{
    protected $fillable=['id', 'name', 'description', 'image', 'active', 'created_by'];
    protected $table='job_sections';
    protected $primaryKey='id';
}
