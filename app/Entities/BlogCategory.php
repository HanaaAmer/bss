<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;


class BlogCategory extends Model
{
    use Translatable;
    public $translatedAttributes = ['name', 'description'];
    protected $fillable=[
        'category_id', 'name', 'description', 'parent_id'
    ];
    protected $table = 'categories';

    protected $primaryKey = 'category_id';
    
    public function blogs()
    {
        return $this->hasMany(Blog::class, 'id', 'category_id');
    }

    
}