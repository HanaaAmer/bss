<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;


class dropdown extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];
    protected $fillable=['id','route'];
    protected $primaryKey='id';
    protected $table='dropdown';
   
}
