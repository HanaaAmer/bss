<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class ImageTranslation extends Model
{
    protected $fillable = ['album_title','long_description'];
    protected $table='image_translation';
}
