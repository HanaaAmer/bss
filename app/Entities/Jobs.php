<?php

namespace App\Entities;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    use Translatable;
    public $translatedAttributes = ['name','title', 'description','requirements','requirednumber'];
    protected $fillable=['id','companys_id','image','active','show_in_homepage'];
    protected $table='jobs';
    protected $primaryKey='id';
    public function companys()
    {
        return $this->belongsTo(companys::class,'companys_id','id');
    }
    protected $casts = [
        'requirements' => 'array'
      ];
      
}
