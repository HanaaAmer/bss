<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class progressofthejob extends Model
{
    protected $fillable=['id', 'name', 'email', 'phone', 'cv','jobs_id'];
    protected $primaryKey='id';
    protected $table='progressofthejob';
    public function Jobs()
    {
        return $this->belongsTo(Jobs::class,'jobs_id','id');
    }
}
