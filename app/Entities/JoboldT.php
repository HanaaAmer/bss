<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class JobTranslation extends Model
{
    protected $fillable=['name','short_description','long_description'];
    protected $table='jobs_translation';
   
}
