<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class BlogCategoryTranslation extends Model
{
    protected $fillable = ['name', 'description'];
    protected $table='categories_translation';
}