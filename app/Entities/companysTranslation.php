<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class companysTranslation extends Model
{
   protected $fillable=['name','title','description'];
    protected $table='companys_translation';
   
}