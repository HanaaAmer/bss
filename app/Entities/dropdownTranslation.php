<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class dropdownTranslation extends Model
{
    protected $fillable=['name'];
    protected $table='dropdown_translation';
   
}