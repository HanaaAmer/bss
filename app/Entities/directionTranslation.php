<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class directionTranslation extends Model
{
    protected $fillable=['name'];
    protected $table='direction_translation';
   
}