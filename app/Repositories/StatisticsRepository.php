<?php

namespace App\Repositories;

use App\Entities\Jobs;
use App\Entities\User;

use App\Entities\Page;
use App\Entities\Contact;
use App\Entities\BlogCategory;
use App\Entities\Blog;
use App\Entities\Tracker;
use Auth;
use File;

class StatisticsRepository extends BaseRepository
{
    protected $job;
    protected $user;
    protected $client;
    protected $page;
    public function __construct()
    {
        $this->job=new Jobs();
        $this->user=new User();
        $this->client=new Client();
        $this->page=new Page();
    }

    public static function countJobs()
    {
        return count(Jobs::all());
    }

    public static function countPages()
    {
        return count(Page::all());
    }
    public static function countContacts()
    {
        return count(Contact::all());
    }

    public static function countUsers()
    {
        return count(User::all());
    }

   
    public static function countCategories()
    {
        return count(BlogCategory::all());
    }
    public static function countBlogs()
    {
        return count(Blog::all());
    }
    public static function countVisitors()
    {
        return count(Tracker::all());
    }
    public static function getLastVisitor()
    {
        return Tracker::orderBy('id','desc')->first();
    }
    public static function getAllVisits()
    {
        return Tracker::orderBy('id','desc')->get();
    }



    
}