<?php

namespace App\Repositories;
use File;
use App\Entities\companys;
use App\Entities\Page;
use App\Entities\companysTranslation;
class companysRepository extends BaseRepository
{
    protected $company;

    public function __construct()
    {
        $this->company = new companys();
        $this->companytranslation= new companysTranslation();
    }

    public function getAllcompany()
    {
        return $this->getAllItems($this->company);
    }

    public function postAddcompany($data, $company)
    {
     
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/companys';
            $file->move($destinationPath, $picture);
            $company->fill(['image'=>$picture]);
        }
        if ($data->hasFile('logo'))
        {
            $file= $data->file('logo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/companys';
            $file->move($destinationPath, $picture);
            $company->fill(['logo'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $company->{"name:$locale"}   = $data->input("name:{$locale}");
            $company->{"description:$locale"}   = $data->input("description:{$locale}");
           
            $company->{"title:$locale"}   = $data->input("title:{$locale}");
           
        }
      
        $company->fill(['path' => $data->path]);
        $company->fill(['direction_id' => $data->direction_id]);
        $company->save();

        return $company;
    }

    public function getcompanyById($companyId)
    {
        return $this->getItemById($companyId, $this->company);
    }

    public function updatecompanyById($companyId, $data)
    {
      
        $company = $this->company->find($companyId);
        if ($data->hasFile('image'))
        {
            $photoName=$company->image;
            File::delete('public/assets/images/companys/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/companys';
            $file->move($destinationPath, $picture);
            $company->fill(['image'=>$picture]);
        }
        if ($data->hasFile('logo'))
        {
            $photoName=$company->image;
            File::delete('public/assets/images/companys/'.$photoName);
            $file= $data->file('logo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/companys';
            $file->move($destinationPath, $picture);
            $company->fill(['logo'=>$picture]);
        }
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $company->{"name:$locale"}   = $data->input("name:{$locale}");
            $company->{"description:$locale"}   = $data->input("description:{$locale}");
          
            $company->{"title:$locale"}   = $data->input("title:{$locale}");
          
        }
        $company->fill(['path' => $data->path]);
        $company->fill(['direction_id' => $data->direction_id]);
        $company->save();

        return $company;
    }

    public function deletecompanyById($companyId)
    {
       
       
       
      
           $companytranslation = companysTranslation::all()->where('companys_id', '=', $companyId);
          foreach($companytranslation as $translation){
       $this->deleteItemById($translation->id, $this->companytranslation);
          }
        $company = $this->company->find($companyId);
        $this->deleteItemById($companyId, $this->company);
        
    }
    static public function getcompanys()
    {
        return companys::all();;
    }
    static public function getwelcome()
    {
        return Page::find(1);
    }
    static public function getsubscrib()
    {
        return Page::find(3);
    }
  
}
