<?php

namespace App\Repositories;

use App\Entities\ImagesComments;

use File;

class ImagesCommentsRepository extends BaseRepository
{
    protected $ImagesComments;
    public function __construct()
    {
        $this->ImagesComments=new ImagesComments();
    }
    public function getAllImagesComments()
    {
        return $this->getAllItems($this->ImagesComments);
    }
   
    public function postAddImagesComments($data,$ImagesComments)
    {
        
        $ImagesComments->fill(['name'=>$data->name]);
        $ImagesComments->fill(['email'=>$data->email]); 
        $ImagesComments->fill(['comment'=>$data->comment]);      
        $ImagesComments->fill(['active'=>$data->active]);
        $ImagesComments->fill(['image_id'=>$data->image_id]);
        $ImagesComments->save();
        return $ImagesComments;
    }
    public function getImagesCommentsById($ImagesCommentsId)
    {
        return $this->getItemById($ImagesCommentsId,$this->ImagesComments);
    }
   
    public function deleteImagesCommentsById($ImagesCommentsId)
    {
        $ImagesComments=$this->ImagesComments->find($ImagesCommentsId);      
        $this->deleteItemById($ImagesCommentsId,$this->ImagesComments);
    }

    static public function ImagesCommentsToView($ImageId)
    {
        return ImagesComments::where('image_id',$ImageId)->get();
    }
    
}