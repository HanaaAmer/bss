<?php

namespace App\Repositories;

use App\Entities\progressofthejob;
use File;
use Illuminate\Support\Facades\Auth;

/**
 * Class ContactRepository
 * @package App\Repositories
 */
class SubscriptionRepository extends BaseRepository
{
    protected $subscription;
    /**
     * ContactRepository constructor.
     */
    public function __construct()
	{
        $this->subscription= new progressofthejob();
    }

    public function getAllSubscription()
    {
        return $this->getAllItems($this->subscription);
    }

    public function AddSubscription($request,$subscription){
        if ($request->file('cv'))
        {   
            $file= $request->file('cv');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $cv = date('His').$filename;
            $destinationPath='uploads/subscription';
            $file->move($destinationPath,$cv);
            $subscription->fill(['cv'=>$cv]);
          
        }
        $subscription->fill(['name'=>$request->name]);
        $subscription->fill(['email'=>$request->email]);
        $subscription->fill(['phone'=>$request->phone]);
        $subscription->fill(['cv'=>$request->cv]);
        $subscription->fill(['jobs_id'=>$request->jobs_id]);
        $subscription->save();
        return $subscription;
    }
    public function getSubscriptionById($subscription_id)
    {
        return $this->getItemById($subscription_id,$this->subscription);
    }

    public function deleteSubscriptionById($subscription_id)
    {   
        return $this->deleteItemById($subscription_id,$this->subscription);
    }

}