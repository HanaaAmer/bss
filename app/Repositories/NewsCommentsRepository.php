<?php

namespace App\Repositories;

use App\Entities\NewsComments;

use File;

class NewsCommentsRepository extends BaseRepository
{
    protected $NewsComments;
    public function __construct()
    {
        $this->NewsComments=new NewsComments();
    }
    public function getAllNewsComments()
    {
        return $this->getAllItems($this->NewsComments);
    }
   
    public function postAddNewsComments($data,$NewsComments)
    {
        
        $NewsComments->fill(['name'=>$data->name]);
        $NewsComments->fill(['email'=>$data->email]); 
        $NewsComments->fill(['comment'=>$data->comment]);      
        $NewsComments->fill(['active'=>$data->active]);
        $NewsComments->fill(['news_id'=>$data->news_id]);
        $NewsComments->save();
        return $NewsComments;
    }
    public function getNewsCommentsById($NewsCommentsId)
    {
        return $this->getItemById($NewsCommentsId,$this->NewsComments);
    }
   
    public function deleteNewsCommentsById($NewsCommentsId)
    {
        $NewsComments=$this->NewsComments->find($NewsCommentsId);      
        $this->deleteItemById($NewsCommentsId,$this->NewsComments);
    }
    static public function NewsCommentsToView($NewsId)
    {
        return NewsComments::where('news_id',$NewsId)->get();
    }
   
}