<?php

namespace App\Repositories;

use App\Entities\JobSections;
use Auth;
use File;

class JobSectionRepository extends BaseRepository
{
    protected $JobSection;
    public function __construct()
    {
        $this->jobSection=new JobSections();
    }
    public function getAllJobSections()
    {
        return $this->getAllItems($this->jobSection);
    }

    public function postAddJobSections($data,$sectionJob)
    {
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/jobs/sections';
            $file->move($destinationPath, $picture);
            $sectionJob->fill(['image'=>$picture]);
        }
        $sectionJob->fill(['name'=>$data->name]);
        $sectionJob->fill(['description'=>$data->description]);
        $sectionJob->fill(['active'=>$data->active]);
        $sectionJob->fill(['created_by'=>Auth::user()->id]);
        $sectionJob->save();
        return $sectionJob;

    }
    public function getSectionJobById($jobSectionId)
    {
        return $this->getItemById($jobSectionId,$this->jobSection);
    }

    public function updateSectionJobById($jobSectionId,$data)
    {
        $sectionJob=$this->jobSection->find($jobSectionId);
        if ($data->hasFile('image'))
        {
            $photoName=$sectionJob->image;
            File::delete('uploads/jobs/sections/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/jobs/sections';
            $file->move($destinationPath, $picture);
            $sectionJob->fill(['image'=>$picture]);
        }
        $sectionJob->fill(['name'=>$data->name]);
        $sectionJob->fill(['description'=>$data->description]);
        $sectionJob->fill(['active'=>$data->active]);
        $sectionJob->save();
        return $sectionJob;
    }

    public function deleteSectionJobById($jobSectionId)
    {
        $sectionJob=$this->jobSection->find($jobSectionId);
        $photoName=$sectionJob->image;
        File::delete('uploads/jobs/sections/'.$photoName);
        $this->deleteItemById($jobSectionId,$this->jobSection);
    }

    static public function jobSectionsToView()
    {
        return jobSections::all();
    }
}