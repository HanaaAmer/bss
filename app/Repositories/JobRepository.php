<?php

namespace App\Repositories;
use App\Entities\progressofthejob;
use App\Entities\Jobs;
use App\Entities\JobsTranslation;
use Auth;
use File;

class JobRepository extends BaseRepository
{
    protected $job;
    protected $JobTranslation;
    public function __construct()
    {
        $this->job=new Jobs();
        $this->JobTranslation=new JobsTranslation();
        $this->pro=new progressofthejob();
    }
    public function getAllJobs()
    {
        return $this->getAllItems($this->job);
    }

    
    public function postAddJob($data,$job)
    {
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $job->{"name:$locale"}   = $data->input("name:{$locale}"); 
            $job->{"title:$locale"}   = $data->input("title:{$locale}");
            $job->{"description:$locale"}   = $data->input("description:{$locale}");
            $job->{"requirements:$locale"}   = $data->input("requirements:{$locale}"); 
            $job->{"requirednumber:$locale"}   = $data->input("requirednumber:{$locale}"); 
        }
      
        $job->fill(['companys_id'=>$data->companys_id]);
        $job->fill(['active'=>$data->active]);
        $job->fill(['show_in_homepage'=>$data->show_in_homepage]);
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/jobs';
            $file->move($destinationPath, $picture);
            $job->fill(['image'=>$picture]);
        }
        
        $job->save();
        return $job;
    }
    public function getJobById($jobId)
    {
        return $this->getItemById($jobId,$this->job);
    }
    public function updateJobById($jobId,$data)
    {
        $job=$this->job->find($jobId);
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $job->{"name:$locale"}   = $data->input("name:{$locale}"); 
            $job->{"title:$locale"}   = $data->input("title:{$locale}");
            $job->{"description:$locale"}   = $data->input("description:{$locale}");
            $job->{"requirements:$locale"}   = $data->input("requirements:{$locale}");  
            $job->{"requirednumber:$locale"}   = $data->input("requirednumber:{$locale}"); 
        }
      
        $job->fill(['companys_id'=>$data->companys_id]);
        $job->fill(['active'=>$data->active]);
        $job->fill(['show_in_homepage'=>$data->show_in_homepage]);
        if ($data->hasFile('image'))
        {
            $photoName=$job->image;
            File::delete('public/assets/images/jobs/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/jobs';
            $file->move($destinationPath, $picture);
            $job->fill(['image'=>$picture]);
        }
        $job->save();
        return $job;      
    }

    public function deleteJobById($jobId)
    {
        $JobTranslation = JobsTranslation::all()->where('jobs_id', '=', $jobId);
        foreach($JobTranslation as $translation){
         $this->deleteItemById($translation->id, $this->JobTranslation);
        }
        $job=$this->job->find($jobId);
        $photoName=$job->image;
        File::delete('public/assets/images/jobs/'.$photoName);
        $this->deleteItemById($jobId,$this->job);
    }

    public function getTopJobs()
    {
        return Jobs::orderBy('id','desc')->where('active',1)->where('show_in_homepage',1)->take(3)->get();
    }

    public function getJobDetailsByJobId($jobId)
    {
        return $this->getItemById($jobId,$this->job);
    }

    static public function jobToView()
    {
        return Jobs::orderBy('id','desc')->where('active',1)->get();
    }
    public function deleteprogessById($proId)
    {
        $pro=progressofthejob::find($proId);
        $this->deleteItemById($proId,$this->pro);
    }
    static public function getlatestjob()
    {
        return Jobs::orderBy('id','desc')->take(3)->get();
    }
}