<?php

namespace App\Repositories;

use App\Entities\News;
use Auth;
use File;

class NewsRepository extends BaseRepository
{
    protected $News;
    public function __construct()
    {
        $this->News=new News();
    }
    public function getAllNews()
    {
        return $this->getAllItems($this->News);
    }
    
    public function postAddNews($data,$News)
    {
        
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/News';
            $file->move($destinationPath, $picture);
            $News->fill(['image'=>$picture]);
        }
        if ($data->hasFile('titleImage'))
        {
            $file= $data->file('titleImage');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/News';
            $file->move($destinationPath, $picture);
            $News->fill(['titleImage'=>$picture]);
        }
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $News->{"title:$locale"}   = $data->input("title:{$locale}");
            $News->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $News->{"long_description:$locale"} = $data->input("long_description:{$locale}");

        }
       
        $News->fill(['active'=>$data->active]);
        $News->fill(['created_by'=>Auth::user()->id]);
        $News->save();
        return $News;
    }
    public function getNewsById($NewsId)
    {
        return $this->getItemById($NewsId,$this->News);
    }
    public function updateNewsById($NewsId,$data)
    {
        $News=$this->News->find($NewsId);
        if ($data->hasFile('image'))
        {
            $photoName=$News->image;
            File::delete('uploads/News/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/News';
            $file->move($destinationPath, $picture);
            $News->fill(['image'=>$picture]);
        }
        if ($data->hasFile('titleImage'))
        {
            $photoName=$News->titleImage;
            File::delete('uploads/News/'.$photoName);
            $file= $data->file('titleImage');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/News';
            $file->move($destinationPath, $picture);
            $News->fill(['titleImage'=>$picture]);
        }
       
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $News->{"title:$locale"}   = $data->input("title:{$locale}");
            $News->{"short_description:$locale"} = $data->input("short_description:{$locale}");
            $News->{"long_description:$locale"} = $data->input("long_description:{$locale}");
        }
        $News->fill(['active'=>$data->active]);
        $News->save();
        return $News;
    }

    public function deleteNewsById($NewsId)
    {
        
        $News=$this->News->find($NewsId);
        $photoName=$News->image;
        File::delete('uploads/News/'.$photoName);  
        $titleImageName=$News->titleImage; 
        File::delete('uploads/News/'.$titleImageName);    
        $this->deleteItemById($NewsId,$this->News);

   
    }
    static public function getTopNews()
    {
        return News::orderBy('id','desc')->where('active',1)->take(5)->get();
    }


    static public function NewsToView()
    {
        return News::orderBy('id','desc')->where('active',1)->get();
    }
    static public function getlatestNews()
    {
        return News::orderBy('id','desc')->take(3)->get();
    }
}