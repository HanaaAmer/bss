<?php

namespace App\Repositories;

use App\Entities\Image;

use File;

class ImageRepository extends BaseRepository
{
    protected $image;
    public function __construct()
    {
        $this->image=new Image();
    }
    public function getAllImage()
    {
        return $this->getAllItems($this->image);
    }
   
    public function postAddImage($data,$image)
    {
        $files=[];
        if ($data->hasFile('image'))
        {            
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];          
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/images';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);

            }    
        }
      
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $image->{"album_title:$locale"} = $data->input("album_title:{$locale}");
            $image->{"long_description:$locale"} = $data->input("long_description:{$locale}");
        } 
        $image->fill(['image'=>$files]);
        $image->save();
        return $image;
    }
    public function getImageById($imageId)
    {
    
        return $this->getItemById($imageId,$this->image);
    }
    public function updateImageById($imageId,$data)
    {
        // return $data;
        $image=$this->image->find($imageId);
       $files=[];
        $photoName=[];
        
            $photoName=$image->image;
            if ($data->hasFile('image'))
        {
           
            for ($x = 0; $x < count($data->image) ; $x++)
            {
            $file= $data->image[$x];  
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/images';
            $file->move($destinationPath, $picture);
            array_push($files, $picture);
            }
        }
        $files = array_merge($files,$photoName) ;  
        foreach (\Config::get('languages') as $locale=>$language) 
        {
            $image->{"album_title:$locale"} = $data->input("album_title:{$locale}");
            $image->{"long_description:$locale"} = $data->input("long_description:{$locale}");
        } 
        $image->fill(['image'=>$files]);
        $image->save();
        return $image;
    }

    public function deleteImageById($imageId)
    {
        $image=$this->image->find($imageId);
        $photoName=$image->image;
        for($i=0; $i<count($photoName);$i++){
            File::delete('uploads/images'.$photoName[$i]);
            }
        $this->deleteItemById($imageId,$this->image);
    }
    static public function getlatestImages()
    {
        return Image::orderBy('id','desc')->take(3)->get();
    }
   
}