<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Entities\User;
use App\Entities\UserRoles;
use Illuminate\Support\Facades\Auth;
use DB;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends BaseRepository
{
    protected $user;
    /**
     * UserRepository constructor.
     */
    public function __construct()
	{
        $this->user=new User();
        $this->role=new UserRoles();
    }
    /**
     * @return array of all users
     */
    public function getAllUsers()
    {
        return $this->getAllItems($this->user);
    }

    public function postAddUser($data,$user)
    {
        if ($data->file('user_cv'))
        {   
            $file= $data->file('user_cv');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $cv = date('His').$filename;
            $destinationPath='uploads/users/cv';
            $file->move($destinationPath, $cv);
            $user->user_cv=$cv; 
        }
        if ($data->hasFile('image'))
        {   
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/users';
            $file->move($destinationPath, $picture);
            $user->image=$picture; 
        }
            $user->email=$data['email'];
            $user ->password =bcrypt($data['password']);
            $user->user_first_name=$data['user_first_name'];
            $user->user_last_name=$data['user_last_name'];
            $user->country=$data['country'];
            $user->city=$data['city'];
            $user->address=$data['address'];
            $user->phone=$data['phone'];
            $user->role_id=$data['role_id'];
            $user->education=$data['education'];
            $user->experience=$data['experience'];
            $user->skills_title=serialize($data['skills_title']);
            $user->skills_degree=serialize($data['skills_degree']);
            $user->facebook_link=$data['facebook_link'];
            $user->linkedin_link=$data['linkedin_link'];
            $user->twitter_link=$data['twitter_link'];
            $user->google_link=$data['google_link'];
            $user->company_name=$data['company_name'];
            $user->bussiness_field=$data['bussiness_field'];
            $user->official_website=$data['official_website'];
            $user->about_company=$data['about_company'];
            $user->save();
            return $user;   
    }
    public function getUserById($userId)
    {
        return $this->getItemById($userId,$this->user);
    }

    public function updateUser($userId,$data)
    {
        $user=User::find($userId);
        if ($data->file('user_cv'))
        {   
            $file= $data->file('user_cv');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $cv = date('His').$filename;
            $destinationPath='uploads/users/cv';
            $file->move($destinationPath, $cv);
            $user->user_cv=$cv; 
        }
        if ($data->hasFile('image'))
        {   
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='uploads/users';
            $file->move($destinationPath, $picture);
            $user->image=$picture; 
        }
            $user->email=$data['email'];
            $user ->password =bcrypt($data['password']);
            $user->user_first_name=$data['user_first_name'];
            $user->user_last_name=$data['user_last_name'];
            $user->job_title=$data['job_title'];
            $user->country=$data['country'];
            $user->city=$data['city'];
            $user->address=$data['address'];
            $user->phone=$data['phone'];
            $user->role_id=$data['role_id'];
            $user->education=$data['education'];
            $user->experience=$data['experience'];
            $user->skills_title=serialize($data['skills_title']);
            $user->skills_degree=serialize($data['skills_degree']);
            $user->facebook_link=$data['facebook_link'];
            $user->linkedin_link=$data['linkedin_link'];
            $user->twitter_link=$data['twitter_link'];
            $user->google_link=$data['google_link'];
            $user->company_name=$data['company_name'];
            $user->bussiness_field=$data['bussiness_field'];
            $user->official_website=$data['official_website'];
            $user->about_company=$data['about_company'];
            $user->save();
            return $user;
    }

    public function deleteUserById($userId)
    {
        $this->deleteItemById($userId,$this->user);
    }
    //Roles
    public function getAllRoles()
    {
        return $this->getAllItems($this->role);
    }

    public function postAddRole($data,$role)
    {
        $role->fill(['role_name'=>$data->role_name]);
        if($data->items_access == NULL)
        {
            $role->fill(['items_access'=>serialize(['getAddSlider'])]);
            
        }
        else{
            $role->fill(['items_access'=>serialize($data->items_access)]);

        }
        $role->save(); 
        return $role; 
    }
    public function getroleById($roleId)
    {
        return $this->getItemById($roleId,$this->role);
    }

    public function updateRole($roleId,$data)
    {
        $role=UserRoles::find($roleId);
        $role->fill(['role_name'=>$data->role_name]);
        $role->fill(['items_access'=>serialize($data->items_access)]);
        $role->save();
        return $role;
    }

    public function deleteRoleById($roleId)
    {
        $this->deleteItemById($roleId,$this->role);
    }
   
}