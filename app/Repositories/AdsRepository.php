<?php
namespace App\Repositories;
use Modules\Backend\Ads\Models\AdManagement;

use File;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class AdsRepository extends BaseRepository {
    protected $ads;
  
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->ads=new AdManagement();
       
    }
    /**
     * @return array of all Ads
     */
    public function getAllAds(){
        return $this->getAllItems($this->ads);
    }
    public function getThePositionName(){
        return $this->getAllItems($this->position);
    }
    public function postAddAds($data,$ad){
        $pathToStore = public_path('assets/images/ads/');
        if ($data->hasFile('image'))
        {
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/ads';
            $file->move($destinationPath, $picture);
            $ad->fill(['image'=>$picture]);
        }
           $ad->fill(['link'=>$data->link]);
          // $ad->fill(['positions_id'=>$data->positions_id]);
           $ad->save();
           return $ad;
    }
    public function getAdsById($adId){
       return $this->getItemById($adId,$this->ads);
    }

    public function updateAdById($adId,$data){
        $ad=$this->ads->find($adId);
        $path_store=public_path('assets/images/ads/');
        if ($data->hasFile('image'))
        {
            $photoName=$ad->image;
            File::delete('public/assets/images/ads/'.$photoName);
            $file= $data->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath='public/assets/images/ads';
            $file->move($destinationPath, $picture);
            $ad->fill(['image'=>$picture]);
        }
        $ad->fill(['link'=>$data->link]);
       // $ad->fill(['positions_id'=>$data->positions_id]);
        $ad->save();
        return $ad;
    }
    public function deleteAdsById($adId){
        $ad=$this->ads->find($adId);
        $photoName=$ad->image;
        File::delete('public/assets/images/ads/'.$photoName);
        $this->deleteItemById($adId,$this->ads);
    }
    public function getAdsDetails($adId){
        return $this->getItemById($adId,$this->ads);
    }
   static public function getAds(){
        return AdManagement::all();
    }
}