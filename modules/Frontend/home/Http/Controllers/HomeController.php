<?php

namespace Modules\Frontend\home\Http\Controllers;
use App\Entities\companys;
use App\Entities\Page;
use App\Http\Controllers\Controller;
use Modules\Frontend\home\Requests\HomeRequest;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    
    public function index()
    {
        
      
        $companys=companys::all();
       $item=Page::find(2);
       return view('front.home.home', compact('companys','item'));

    }
   
}
