<?php

namespace Modules\Frontend\Media\Http\Controllers;

use App\Repositories\ImageRepository;
use App\Entities\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\ImagesComments;
use App\Entities\Meta;
use Modules\Frontend\Media\Requests\ImageRequest;
use Illuminate\Database\Eloquent\Collection;
use File;

class Media_FController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllGalleries(ImageRepository $ImageRepo)
    {
        
        $images = $ImageRepo->getAllImage();
        return view('front.media.image', compact('images'));
    }
    
    public function selectGallery($imagelId, ImageRepository $ImageRepo)
    {
        $meta=Meta::where("link_id","=",4)->first();
        $images  = $ImageRepo->getImageById($imagelId);
        $ImagesComments=ImagesComments::where('image_id',$imagelId)->where('active',1)->get();
        return view('front.media.selectedImage', compact('images','ImagesComments','meta'));
    }
  
}
