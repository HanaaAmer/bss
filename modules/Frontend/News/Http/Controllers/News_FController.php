<?php

namespace Modules\Frontend\News\Http\Controllers;

use App\Repositories\NewsRepository;
use App\Entities\News;
use App\Entities\NewsComments;
use App\Entities\Meta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Frontend\News\Requests\NewsRequest;
use File;

class News_FController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllNews(NewsRepository $NewsRepo)
    {
        
        $News = $NewsRepo->getAllNews();

        return view('front.News.news', compact('News'));
    }
    
    public function selectNews($NewsId, NewsRepository $NewsRepo)
    {
        $meta=Meta::where("link_id","=",4)->first();
        $News  = $NewsRepo->getNewsById($NewsId);
        $NewsComments=NewsComments::where('news_id',$NewsId)->where('active',1)->get();
        return view('front.News.selectedNews', compact('News','NewsComments','meta'));
    }
  
}
