<?php
namespace Modules\Frontend\ContactUs\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Repositories\BlogRepository;
use App\Repositories\ContactsRepository;
use App\Repositories\SubscriptionRepository;
use App\Entities\Contact;
use App\Entities\Setting;
use App\Entities\Subscription;
use Modules\Frontend\ContactUs\Requests\SubscriptionRequest;
use Modules\Frontend\ContactUs\Requests\ContactRequest;


class ContactUsController extends Controller
{
    public $blogRepo;
    public $contactRepo;
    public $subscriptionRebo;
    public function __construct()
    {
        $this->blogRepo         = new BlogRepository();
        $this->contactRepo      = new ContactsRepository();
        $this->subscriptionRebo = new SubscriptionRepository();
    }
    public function index(){
        $setting      = Setting::first();
        $breakingNews = $this->blogRepo->breakingNews();
        $mostReadable = $this->blogRepo->mostReadableNews();
        return view('front.contacts.all_contacts',compact('breakingNews', 'mostReadable','setting'));
    }
    public function store(ContactRequest $request,Contact $contact ){
      
        $this->contactRepo->AddContactUs($request,$contact);
        return redirect()->back()->with(['message'=>\App\Http\Controllers\HomeController::translateWord('booking_message')]);

    }
    public function getSubscription(){
         $setting      = Setting::first();
        $breakingNews = $this->blogRepo->breakingNews();
        $mostReadable = $this->blogRepo->mostReadableNews();
        return view('front.tersana.subscript',compact('breakingNews', 'mostReadable','setting'));
    }
    public function postSubscription(SubscriptionRequest $request,Subscription $subscription ){
        
        $this->subscriptionRebo->AddSubscription($request,$subscription);
        return redirect()->back()->with('subscription','تم اشتراكك بنجاح');
    }
}
