<?php
Route::get('/contactus',['uses'=>'ContactUsController@index','as'=>'getFrontContactus']);
Route::post('/contactus',['uses'=>'ContactUsController@store','as'=>'postAddContactus']);
Route::get('/subscript',['uses'=>'ContactUsController@getSubscription','as'=>'GetSubscription']);
Route::post('/subscript',['uses'=>'ContactUsController@postSubscription','as'=>'postSubscription']);
?>