<?php

Route::get('/jobs',['uses'=>'alljobsController@index','as'=>'getjobs']);
Route::get('/jobs/{routId}',['uses'=>'alljobsController@dropindex','as'=>'getdropjobs']);
Route::get('/mostjobs/{routId}',['uses'=>'alljobsController@indexmost','as'=>'getmostjobs']);
Route::get('/openjobs/{routId}',['uses'=>'alljobsController@indexopen','as'=>'getopenjobs']);
Route::get('/search',['uses'=>'alljobsController@search','as'=>'search']);
Route::get('/jobdetails/{jobId}',['uses'=>'alljobsController@jobdetails','as'=>'jobdetails']);
Route::post('/jobdetails',['uses'=>'alljobsController@postAddprogressofthejob','as'=>'postAddprogressofthejob']);

