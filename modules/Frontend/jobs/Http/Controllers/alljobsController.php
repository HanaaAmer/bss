<?php

namespace Modules\Frontend\jobs\Http\Controllers;
use App\Entities\Jobs;
use App\Entities\dropdown;
use App\Entities\JobsTranslation;
use App\Entities\progressofthejob;
use App\Entities\Page;
use App\Entities\Meta;
use DB;
use App\Http\Controllers\Controller;
use Modules\Backend\Jobs\Requests\JobRequest;
use App\Repositories\SubscriptionRepository;
/**
 * Class alljobsController
 * @package App\Http\Controllers
 */
class alljobsController extends Controller
{
    
    public function index()
    {
        $meta=Meta::where("link_id","=",15)->first();
      $dropdown=dropdown::all();
        $Jobs=Jobs::paginate(10);
        $selectedroute=null;
       return view('front.jobs.all_jobs', compact('Jobs','dropdown','selectedroute','meta'));

    }
    public function dropindex($id)
    {
        
      $dropdown=dropdown::all();
        $Jobs=Jobs::paginate(10);
        $selectedroute=$id;
        $meta=Meta::where("link_id","=",15)->first();
       return view('front.jobs.all_jobs', compact('Jobs','dropdown','selectedroute','meta'));

    }
    public function indexmost($id)
    {
        $meta=Meta::where("link_id","=",15)->first();
        $dropdown=dropdown::all();
        $Jobs=Jobs::orderBy('created_at', 'desc')->paginate(10);
     $selectedroute=$id;
    
       return view('front.jobs.all_jobs', compact('Jobs','dropdown','selectedroute','meta'));

    }
    public function indexopen($id)
    {
        $meta=Meta::where("link_id","=",15)->first();
        $dropdown=dropdown::all();
        $Jobs=Jobs::where('active', '=',1)->paginate(10);
        $selectedroute=$id;
       return view('front.jobs.all_jobs', compact('Jobs','dropdown','selectedroute','meta'));

    }
    public function jobdetails($id)
    {
        
        $meta=Meta::where("link_id","=",15)->first();
        $Jobs=Jobs::find($id);
     
       return view('front.jobs.job_details', compact('Jobs','meta'));

    }
    public function postAddprogressofthejob(JobRequest $request,SubscriptionRepository $subrepo,progressofthejob $progress)
    {
       
    
        $subrepo->AddSubscription($request,$progress);
         
        return redirect()->back()->with('sucess', 'Content has been send successfully!');

    }
    public function search(JobRequest $request) {
        $dropdown=dropdown::all();  
         $name = $request->search_name;
         $meta=Meta::where("link_id","=",15)->first();
      $Jobs = JobsTranslation::where([["name", "LIKE","%$name%"],['locale','=','ar']])->paginate(10);
              $selectedroute=null;
             return view('front.jobs.search', compact('Jobs','dropdown','selectedroute','meta'));
        }
     
       
     
    
}
