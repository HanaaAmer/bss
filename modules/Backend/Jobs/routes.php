<?php

Route::group(['prefix'=>'backend-jobs','middleware'=>'auth'],function (){
    //Jobs
    Route::get('/',['uses'=>'JobController@index','as'=>'getAllJobs']);
    Route::get('/add-job',['uses'=>'JobController@create','as'=>'getAddJob']);
    Route::post('/add-job',['uses'=>'JobController@store','as'=>'postAddJob']);
    Route::get('/job/{jobId}',['uses'=>'JobController@edit','as'=>'getJobById']);
    Route::put('/job/{jobId}',['uses'=>'JobController@update','as'=>'updateJobById']);
    Route::post('/job/{jobId}',['uses'=>'JobController@delete','as'=>'deleteJobById']);
    Route::get('/allprogressofthejob',['uses'=>'JobController@allprogressofthejob','as'=>'allprogressofthejob']);
    
    Route::post('/allprogressofthejob/{proId}',['uses'=>'JobController@deleteprogessById','as'=>'deleteprogessById']);
    
    Route::put('/allprogressofthejob',['uses'=>'JobController@updateprogress','as'=>'updateprogress']);
});