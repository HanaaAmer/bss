<?php

namespace Modules\Backend\Jobs\Http\Controllers;
use App\Entities\companys;
use App\Entities\progressofthejob;
use App\Repositories\JobRepository;
use App\Entities\Jobs;
use App\Entities\JobsTranslation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Jobs\Requests\JobRequest;

class JobController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(JobRepository $jobRepo)
    {
        $jobs = $jobRepo->getAllJobs();
        return view('backend.jobs.all_jobs', compact('jobs'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(jobRepository $jobRepo)
    {
        $companys = companys::all(['name','id']);
       
        return view('backend.jobs.add_job', compact('companys'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(jobRequest $request, jobRepository $jobRepo, Jobs $job)
    {
        $jobRepo->postAddjob($request, $job);
        return redirect()->route('getAllJobs');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $jobId
     * @return \Illuminate\Http\Response
     */
    public function edit($jobId, jobRepository $jobRepo)
    {
        $job  = $jobRepo->getjobById($jobId);
        $companys = companys::all(['name','id']);
        $selectedcompany=$job->companys_id;
        return view('backend.jobs.job', compact('job','companys','selectedcompany'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $jobId
     * @return \Illuminate\Http\Response
     */
    public function update($jobId, jobRequest $request, jobRepository $jobRepo)
    {
        $jobRepo->updateJobById($jobId, $request);
        return redirect()->route('getAllJobs');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $jobId
     * @return \Illuminate\Http\Response
     */
    public function delete($jobId, jobRepository $jobRepo)
    {
        $jobRepo->deletejobById($jobId);
        return redirect()->route('getAllJobs');
    }
    public function allprogressofthejob()
    {
        $jobs =progressofthejob::all();
        return view('backend.jobs.allprogressofthejob', compact('jobs'));
    }
    public function deleteprogessById($proId, jobRepository $jobRepo)
    {
        $jobRepo->deleteprogessById($proId);
        return redirect()->route('allprogressofthejob');
    }
     public function updateprogress(jobRequest $request)
    {
        $pro=progressofthejob::find($request->id);
        $pro->active=1;
        $pro->save();
       return redirect()->route('allprogressofthejob')->with('sucess', 'update sucessfully');
    }
}
