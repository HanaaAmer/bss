<?php

namespace Modules\Backend\Settings\Http\Controllers;

use App\Entities\Setting;
use Illuminate\Http\Request;
use App\Repositories\SettingRepository;
use App\Http\Controllers\Controller;
/**
 * Class SettingController
 * @package App\Http\Controllers
 */
class SettingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SettingRepository $settingRepo)
    {
        $setting = Setting::first();
       return view('backend.settings.settings', compact('setting'));
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $data , Setting $setting)
    {
        foreach (\Config::get('languages') as $locale=>$language) 
        {
           
            $setting->{"description:$locale"}   = $data->input("description:{$locale}");
            $setting->{"contactus_description:$locale"}   = $data->input("contactus_description:{$locale}");
            $setting->{"address:$locale"}   = $data->input("address:{$locale}");
            
        }

      $setting->fill(['email'=>$data->email]);
      $setting->fill(['phone'=>$data->phone]);
       $setting->fill(['whatsapp_number'=>$data->whatsapp_number]);
      $setting->fill(['fax'=>$data->fax]);
      $setting->fill(['map_lat'=>$data->map_lat]);
      $setting->fill(['map_lng'=>$data->map_lng]);
      $setting->fill(['link_facebook'=>$data->link_facebook]);
      $setting->fill(['link_twitter'=>$data->link_twitter]);
      $setting->fill(['link_snapchat'=>$data->link_snapchat]);
      $setting->fill(['link_instagram'=>$data->link_instagram]);
      $setting->fill(['link_linkedin'=>$data->link_linkedin]);
      $setting->fill(['link_whatsapp'=>$data->link_whatsapp]);
       $setting->save();
      
        return redirect()->route('getSettings')->with('sucess', 'Content has been Add successfully!');
    }

    /**
     * @param $settingId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($settingId)
    {
       $setting = Setting::find($settingId);
       return view('backend.settings.setting', compact('setting'));
    }
    /**
     * @param $settingId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $data)
    {
       
        $setting = Setting::first();
     
        foreach (\Config::get('languages') as $locale=>$language) 
        {
           
            $setting->{"description:$locale"}   = $data->input("description:{$locale}");
            $setting->{"contactus_description:$locale"}   = $data->input("contactus_description:{$locale}");
            $setting->{"address:$locale"}   = $data->input("address:{$locale}");
        }

      $setting->fill(['email'=>$data->email]);
      $setting->fill(['phone'=>$data->phone]);
        $setting->fill(['whatsapp_number'=>$data->whatsapp_number]);
      $setting->fill(['fax'=>$data->fax]);
      $setting->fill(['map_lat'=>$data->map_lat]);
      $setting->fill(['map_lng'=>$data->map_lng]);
      $setting->fill(['link_facebook'=>$data->link_facebook]);
      $setting->fill(['link_twitter'=>$data->link_twitter]);
      $setting->fill(['link_snapchat'=>$data->link_snapchat]);
      $setting->fill(['link_instagram'=>$data->link_instagram]);
      $setting->fill(['link_linkedin'=>$data->link_linkedin]);
      $setting->fill(['link_whatsapp'=>$data->link_whatsapp]);
       $setting->save();
      
        return redirect()->route('getSettings')->with('sucess', 'Content has been Update successfully!');
      
    }

    /**
     * @param $settingId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($settingId, SettingRepository $settingRepo)
    {
        $settingRepo->deleteSettingById($settingId);
        return redirect()->route('getAllsettings');
    }
   
}
