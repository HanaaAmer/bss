<?php

namespace Modules\Backend\companys\Http\Controllers;
use App\Entities\companys;
use App\Entities\direction;
use DB;
use Illuminate\Http\Request;
use App\Repositories\companysRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\companys\Requests\companyRequest;
/**
 * Class companyController
 * @package App\Http\Controllers
 */
class companysController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function index(companysRepository $companyRepo)
    {
        $company = $companyRepo->getAllcompany();
        return view('backend.companys.all_companys',compact('company'));
    }
  
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */ 
     public function create()
     {
        $direction = direction::all(['name','id']);
         return view('backend.companys.add_company',compact('direction'));
     }
     public function store(companyRequest $request, companysRepository $companyRepo, companys $company)
     {
        
        $companyRepo->postAddcompany($request, $company);
         
         return redirect()->route('getAllcompany')->with('sucess', 'Content has been Add successfully!');
     }
 
    
    /**
     * @param $companyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
  
    public function edit($companyId, companysRepository $companyRepo)
    {
        $company = $companyRepo->getcompanyById($companyId);
        $direction = direction::all(['name','id']);
        $selecteddirection=$company->direction_id;
        return view('backend.companys.edit_company',compact('company','direction','selecteddirection'));
    }
    /**
     * @param $companyId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($companyId, companyRequest $request, companysRepository $companyRepo)
    {
        $companyRepo->updatecompanyById($companyId, $request);

        return redirect()->route('getAllcompany')->with('sucess', 'Content has been updated successfully!');
    }

    /**
     * @param $companyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($companyId, companysRepository $companyRepo)
    {
    
        $companyRepo->deletecompanyById($companyId);
      
        return redirect()->route('getAllcompany')->with('sucess', 'Content has been delete successfully!');
       
    }

}
