<?php

Route::group(['prefix'=>'company'],function (){
//company
Route::get('/',['uses'=>'companysController@index','as'=>'getAllcompany']);
Route::post('/add-company',['uses'=>'companysController@store','as'=>'postAddcompany']);
Route::get('/add-company',['uses'=>'companysController@create','as'=>'getAddcompany']);
    Route::get('/company/{companyId}',['uses'=>'companysController@edit','as'=>'getcompanyById']);
    Route::put('/company/{companyId}',['uses'=>'companysController@update','as'=>'updatecompanyById']);
    Route::post('/company/{companyId}',['uses'=>'companysController@delete','as'=>'deletecompanyById']);
  
   
});
