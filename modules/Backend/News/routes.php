<?php

Route::group(['prefix'=>'backend-News','middleware'=>'auth'],function (){
    //News
    Route::get('/',['uses'=>'NewsController@index','as'=>'getAllNews']);
    Route::post('/add-News',['uses'=>'NewsController@store','as'=>'postAddNews']);
    Route::get('/add-News',['uses'=>'NewsController@create','as'=>'getAddNews']);
    Route::get('/News/{NewsId}',['uses'=>'NewsController@edit','as'=>'getNewsById']);
    Route::put('/News/{NewsId}',['uses'=>'NewsController@update','as'=>'updateNewsById']);
    Route::post('/News/{NewsId}',['uses'=>'NewsController@delete','as'=>'deleteNewsById']);

    //
    Route::get('/Newscomments/{NewsId}',['uses'=>'NewsCommentsController@index','as'=>'getAllNewsComments']);
    Route::post('/add-Newscomments',['uses'=>'NewsCommentsController@store','as'=>'postAddNewsComments']);
    Route::put('/Newscomments/{id}',['uses'=>'NewsCommentsController@activateCommentById','as'=>'updateNewsCommentsById']);
    Route::post('/Newscomments/{id}',['uses'=>'NewsCommentsController@deletecomment','as'=>'deleteNewsCommentsById']);
});

