<?php

namespace Modules\Backend\News\Http\Controllers;

use App\Repositories\NewsRepository;
use App\Entities\News;
use App\Entities\NewsComments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\News\Requests\NewsRequest;

class NewsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(NewsRepository $NewsRepo)
    {
        $News= $NewsRepo->getAllNews();
        return view('backend.News.all_News', compact('News','comments'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(NewsRepository $NewsRepo)
    {
        return view('backend.News.add_News');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request, NewsRepository $NewsRepo, News $News)
    {
    $NewsRepo->postAddNews($request, $News);
      return redirect()->route('getAllNews');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $EquipmentId
     * @return \Illuminate\Http\Response
     */
    public function edit($NewsId, NewsRepository $NewsRepo)
    {
        $News  = $NewsRepo->getNewsById($NewsId);
        return view('backend.News.News', compact('News'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $EquipmentId
     * @return \Illuminate\Http\Response
     */
    public function update($NewsId, NewsRequest $request, NewsRepository $NewsRepo)
    {
        $NewsRepo->updateNewsById($NewsId, $request);
        return redirect()->route('getAllNews');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $EquipmentId
     * @return \Illuminate\Http\Response
     */
    public function delete($NewsId,NewsRepository $NewsRepo)
    {
        
        $NewsRepo->deleteNewsById($NewsId);
        return redirect()->route('getAllNews');
    }
    
}
