<?php

namespace Modules\Backend\News\Http\Controllers;

use App\Entities\Blog;
use App\Entities\NewsComments;
use Illuminate\Http\Request;
use App\Repositories\NewsCommentsRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\News\Requests\NewsCommentsRequest;
/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class NewsCommentsController extends Controller
{
   
    public function index($NewsId, NewsCommentsRepository $NewsCommentsRepo)
    {
        $comments = NewsComments::where("news_id","=",$NewsId)->get();
        return view('backend.News.comments.index', compact('comments'));
    }

  
    public function store(NewsCommentsRequest $request, NewsCommentsRepository $NewsCommentsRepo, NewsComments $NewsComments)
    {
        $NewsCommentsRepo->postAddNewsComments($request, $NewsComments);
        return redirect()->back()->with('sucess','Thanks');
    }

   
    public function deletecomment($NewsId, NewsCommentsRepository $NewsCommentsRepo)
    {
        $comments = $NewsCommentsRepo->deleteNewsCommentsById($NewsId);
        print_r($comments);
        return redirect()->back()->with('sucess','delete sucessfully');
    }

    public function activateCommentById( $id)
    {
        $comment = NewsComments::find($id);
        if($comment->active!=1)
        {
            $comment->active=1;
        }else{
            $comment->active=0; 
        }
        $comment->save();
        
        return redirect()->back()->with('sucess','update sucessfully');
    }
}
