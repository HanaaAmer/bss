<?php

namespace Modules\Backend\Media\Http\Controllers;

use App\Repositories\ImageRepository;
use App\Entities\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Backend\Media\Requests\ImageRequest;
use File;

class ImagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ImageRepository $ImageRepo)
    {
        
        $images = $ImageRepo->getAllImage();
       
        return view('backend.media.images.index', compact('images'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ImageRepository $ImageRepo)
    {
        $images = $ImageRepo->getAllImage();
      
        return view('backend.media.images.create', compact('images'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request, ImageRepository $ImageRepo, Image $image)
    {
        $ImageRepo->postAddImage($request, $image);
        return redirect()->route('imagesIndex')->with('sucess', 'add sucessfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function edit($imagelId, ImageRepository $ImageRepo)
    {
        $images  = $ImageRepo->getImageById($imagelId);
        return view('backend.media.images.edit', compact('images'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function update($imagelId, ImageRequest $request, ImageRepository $ImageRepo)
    {
     $ImageRepo->updateImageById($imagelId, $request);
         return redirect()->route('imagesIndex')->with('sucess', 'update sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $serviceId
     * @return \Illuminate\Http\Response
     */
    public function delete($imagelId, ImageRepository $ImageRepo)
    {
        $ImageRepo->deleteImageById($imagelId);
        return redirect()->route('imagesIndex')->with('sucess', 'delete sucessfully');
    }
      public function deleteSingle($image, $imagelId)
    {
         
   File::delete('uploads/images/'.$image);
         
   $post=Image::find($imagelId);
   $images=$post->image;
   $_image=[];
   $_image[]=$image;
   $post->image=array_values(array_diff($images,$_image));
   $post->save();
   return redirect()->back();

    }
}

