<?php

namespace Modules\Backend\Media\Http\Controllers;

use App\Entities\Blog;
use App\Entities\ImagesComments;
use Illuminate\Http\Request;
use App\Repositories\ImagesCommentsRepository;
use App\Http\Controllers\Controller;
use Modules\Backend\Media\Requests\ImagesCommentsRequest;
/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class ImagesCommentsController extends Controller
{
   
    public function index($ImageId, ImagesCommentsRepository $ImagesCommentsRepo)
    {
        $comments = ImagesComments::where("image_id","=",$ImageId)->get();
        return view('backend.media.images.comments.index', compact('comments'));
    }

  
    public function store(ImagesCommentsRequest $request, ImagesCommentsRepository $ImagesCommentsRepo, ImagesComments $ImagesComments)
    {
        $ImagesCommentsRepo->postAddImagesComments($request, $ImagesComments);
        return redirect()->back()->with('sucess','Thanks');
    }

   
    public function delete($imageId, ImagesCommentsRepository $ImagesCommentsRepo)
    {
        $comments = $ImagesCommentsRepo->deleteImagesCommentsById($imageId);
        return redirect()->back()->with('sucess','delete sucessfully');
    }

    public function activateCommentById( $id)
    {
        $comment = ImagesComments::find($id);
        if($comment->active!=1)
        {
            $comment->active=1;
        }else{
            $comment->active=0; 
        }
        $comment->save();
        
        return redirect()->back()->with('sucess','update sucessfully');
    }
}
